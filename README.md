Aplicación en C# de automatización de búsqueda de empleo en milanuncios:

1) La aplicación realiza un barrido periodico de los anuncios de dicha web que satisfacen los criterios del usuario en la búsqueda del empleo (Crawling web) creando una base de datos local de los anuncios que satisfacen las necesidades del cliente.

2) El usuario configura las respuestas automatizadas a los anuncios encontrados: Autoenvío de currículum, alerta por teléfono o email.

La aplicación permite que el usuario sea el primero en enviar su candidatura a las empresas objetivo: Este suceso ha demostrado suponer un incremento considerable en las probabilidades de contratación.

