﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public partial class explorerPopup : Form
{
    string explorerUrl;
    public explorerPopup(string url)
    {
        InitializeComponent();
        explorerUrl = url;
    }

    private void OnLoad(object sender, EventArgs e)
    {
        // Al cargar el form navegar a la web pasada en el constructor
        if (explorerUrl != "")
            netWindowPopup.Navigate(explorerUrl);
    }

}

