﻿using System;

/* LENGUAJES
0  = Spanish,
1  = English,
 */

// El sistema funciona por arrays, cada fila es un lenguaje y cada columna es una palabra, así bien las mismas palabras se traducen en columnas para cada lenguaje,
// el indice de cada columna corresponde al idioma según su posición en el menu strip y está también reflejado en la tabla posterior a este comentario.

class Translates
{
    // MENU STRIP
    public static string[,] stripStringNames = new string[2, 6]
    {
        {"Cuenta","Lenguaje","Ayuda","Login","Gestión de Cuenta","Acerca de"},
        {"Account","Languaje","Help","Login","Account management","About"}
    };

    // TEXTOS DE SECCIÓN DE BÚSQUEDA
    public static string[,] searchStringNames = new string[2, 6]
    {
        {"Opciones de búsqueda","Intervalo de búsqueda","Máximo tiempo admitido","Web a buscar","Palabras clave y ciudades a buscar:","segundos"},
        {"Search Options","Search interval","Maxium allowed time","Web where search","Tags and cities to search:","seconds"}
    };

    // TEXTOS DE SECCIÓN DE RESPUESTA
    public static string[,] responsesStringNames = new string[2, 10]
    {
        {"Opciones de respuesta","Emitir Alarma","Avisar Email","Destacar","Responder a oferta","Nombre","Email","Telefono","Asunto","Mensaje"},
        {"Response options","Notify Alarm","Notify Email","Highlight","Response to offer","Name","Email","Phone","Subject","Message"}
    };

    // TEXTOS DE SECCIÓN DE FILTROS
    public static string[,] filterStringNames = new string[2, 8]
    {
        {"Filtros del Log","Palabras clave:","Hasta:","Días.","Máximos:","Filtrar","Mostrar sólo de ciudades buscadas.", "Configuración del display"},
        {"Log Filters","Keywords search:","To:","Days.","Maxium","Filter","Show only looking cities.", "Display configuration"}
    };

    // COLUMNAS DE LOG
    public static string[,] logStringNames = new string[2, 6]
    {
        {"Referencia","Oferta","Descripción","Tiempo","Estado","Claves"},
        {"Reference","Offer","Description","Time","State","Tags"}
    };

    // MENU STRIP DEL RIGHT CLICK
    public static string[,] menuStripStringNames = new string[2, 3]
    {
        {"Ver anuncio","AutoRespuesta","Respuesta manual"},
        {"Show advert","AutoResponse","Response advert"}
    };


    // TEXTOS DINÁMICOS
    public static string[,] dynStrings = new string[2, 17]
    {
        {"En espera","Alertado","Respondido","Ignorado", "Destacado","Día(s)","Hora(s)","Min(s)","Sec(s)","Hace un instante","Desconocido","AutoRenueva", "Escribir palabras separadas por comas","sonido emitido","email enviado","anuncio destacado", "Escribir ciudades separadas por comas"},
        {"Waiting","Alerted","Responsed","Ignored", "Highlighted","Day(s)","Hour(s)","Min(s)","Sec(s)","Just now","Unknown","AutoRenew", "Write tags separated by commas","sonido played","email sent","advert highlighted", "Write cities separated by commas"}
    };

    // TEXTOS DE DISPLAY
    public static string[,] displayStrings = new string[2, 8]
    {
        {"Leyendo ofertas","No se ha seleccionado ninguna web","Esperando eventos","Guardando ofertas","Enviando email de alerta:","Destacando anuncio:","Alerta de anuncio:","Buscando anuncios relevantes"},
        {"Reading offers","No web has been selected","Waiting for events","Saving offers","Sending alert email:","Highlighting advertisement:","Advertisement alert:","Looking for relevant adverts"}
    };

    // TEXTOS DE ERROR
    public static string[,] errorStrings = new string[2, 9]
    {
        {"El intervalo de búsqueda no es válido.","El máximo tiempo admitido no es válido.","No has seleccionado ninguna web.","El mail introducido no es válido.","El movil introducido no es válido.","Introduzca su nombre si desea que la auto respuesta se pueda realizar.","Introduzca un email válido si desea que la auto respuesta se pueda realizar.","Introduzca el asunto del mensaje si desea que la auto respuesta se pueda realizar.","Introduzca el cuerpo del mensaje si desea que la auto respuesta se pueda realizar."},
        {"El intervalo de búsqueda no es válido.","El máximo tiempo admitido no es válido.","No has seleccionado ninguna web.","El mail introducido no es válido.","El movil introducido no es válido.","Introduzca su nombre si desea que la auto respuesta se pueda realizar.","Introduzca un email válido si desea que la auto respuesta se pueda realizar.","Introduzca el asunto del mensaje si desea que la auto respuesta se pueda realizar.","Introduzca el cuerpo del mensaje si desea que la auto respuesta se pueda realizar."}
    };
}