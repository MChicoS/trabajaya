﻿partial class MainForm
{
    /// <summary>
    /// Variable del diseñador requerida.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Limpiar los recursos que se estén utilizando.
    /// </summary>
    /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Código generado por el Diseñador de Windows Forms

    /// <summary>
    /// Método necesario para admitir el Diseñador. No se puede modificar
    /// el contenido del método con el editor de código.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.botStateBar = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.eventDisplay = new System.Windows.Forms.Label();
            this.eventType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Event = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.From = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.To = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.logDisplay = new ListViewNF();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.logConf = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.filterCities = new System.Windows.Forms.CheckBox();
            this.daysFilterLabel = new System.Windows.Forms.Label();
            this.maxRowsLogFilter = new System.Windows.Forms.MaskedTextBox();
            this.FilterOptionsLabel = new System.Windows.Forms.Label();
            this.tagsLogFilter = new System.Windows.Forms.TextBox();
            this.tagsFilterLabel = new System.Windows.Forms.Label();
            this.DateLogFilter = new System.Windows.Forms.MaskedTextBox();
            this.toFilterLabel = new System.Windows.Forms.Label();
            this.maxiumFilterLabel = new System.Windows.Forms.Label();
            this.logButtonRefresh = new System.Windows.Forms.Button();
            this.generalControlButton = new System.Windows.Forms.Button();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.MainWindowTopMenu = new System.Windows.Forms.MenuStrip();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestiónDeCuentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languajeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spanishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rightClickLogMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.verAnuncioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autorespuestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.respuestaManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel5 = new System.Windows.Forms.Panel();
            this.citiesBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.maxDispersionTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.searchOption2Label = new System.Windows.Forms.Label();
            this.maxUpdateTime = new System.Windows.Forms.MaskedTextBox();
            this.netWindow = new System.Windows.Forms.WebBrowser();
            this.searchOptionsLabel = new System.Windows.Forms.Label();
            this.selectedWeb = new System.Windows.Forms.ComboBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.responseMessage = new System.Windows.Forms.TextBox();
            this.wordTags = new System.Windows.Forms.TextBox();
            this.subjectlabel = new System.Windows.Forms.Label();
            this.searchOption1Label = new System.Windows.Forms.Label();
            this.responseSubject = new System.Windows.Forms.TextBox();
            this.secondsLabel = new System.Windows.Forms.Label();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.searchOption3Label = new System.Windows.Forms.Label();
            this.responsePhone = new System.Windows.Forms.TextBox();
            this.searchOption4Label = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.responseMail = new System.Windows.Forms.TextBox();
            this.responseOptionsLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.alarmCheck = new System.Windows.Forms.CheckBox();
            this.responseName = new System.Windows.Forms.TextBox();
            this.mailCheck = new System.Windows.Forms.CheckBox();
            this.autoRepplyCheck = new System.Windows.Forms.CheckBox();
            this.mailBox = new System.Windows.Forms.TextBox();
            this.destacarCheck = new System.Windows.Forms.CheckBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.displayTimer = new System.Windows.Forms.Timer(this.components);
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel7.SuspendLayout();
            this.MainWindowTopMenu.SuspendLayout();
            this.rightClickLogMenuStrip.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // botStateBar
            // 
            this.botStateBar.Location = new System.Drawing.Point(6, 43);
            this.botStateBar.Name = "botStateBar";
            this.botStateBar.Size = new System.Drawing.Size(307, 11);
            this.botStateBar.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Location = new System.Drawing.Point(255, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2, 520);
            this.panel1.TabIndex = 41;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.eventDisplay);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 490);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(940, 25);
            this.panel3.TabIndex = 43;
            // 
            // eventDisplay
            // 
            this.eventDisplay.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.eventDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.eventDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventDisplay.Font = new System.Drawing.Font("Arial", 12.25F);
            this.eventDisplay.Location = new System.Drawing.Point(0, 0);
            this.eventDisplay.Name = "eventDisplay";
            this.eventDisplay.Size = new System.Drawing.Size(940, 25);
            this.eventDisplay.TabIndex = 0;
            this.eventDisplay.Text = "En espera de activación.";
            this.eventDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // eventType
            // 
            this.eventType.Text = "Type";
            this.eventType.Width = 130;
            // 
            // Event
            // 
            this.Event.Text = "Event Log";
            this.Event.Width = 600;
            // 
            // From
            // 
            this.From.Text = "From";
            this.From.Width = 129;
            // 
            // To
            // 
            this.To.Text = "To";
            this.To.Width = 112;
            // 
            // Date
            // 
            this.Date.Text = "Date";
            this.Date.Width = 360;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::TrabajaHOY.Properties.Resources.brightYellow;
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.logDisplay);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.generalControlButton);
            this.panel4.Controls.Add(this.botStateBar);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Location = new System.Drawing.Point(257, 25);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(940, 515);
            this.panel4.TabIndex = 45;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel9.Location = new System.Drawing.Point(0, 60);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1200, 2);
            this.panel9.TabIndex = 48;
            // 
            // logDisplay
            // 
            this.logDisplay.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.logDisplay.BackColor = System.Drawing.Color.OldLace;
            this.logDisplay.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader0,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader5});
            this.logDisplay.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.logDisplay.FullRowSelect = true;
            this.logDisplay.GridLines = true;
            this.logDisplay.Location = new System.Drawing.Point(0, 60);
            this.logDisplay.Name = "logDisplay";
            this.logDisplay.Size = new System.Drawing.Size(940, 430);
            this.logDisplay.TabIndex = 44;
            this.logDisplay.UseCompatibleStateImageBehavior = false;
            this.logDisplay.View = System.Windows.Forms.View.Details;
            this.logDisplay.MouseClick += new System.Windows.Forms.MouseEventHandler(this.logDisplayClicked);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "";
            this.columnHeader7.Width = 32;
            // 
            // columnHeader0
            // 
            this.columnHeader0.Text = "Referencia";
            this.columnHeader0.Width = 80;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Oferta";
            this.columnHeader1.Width = 306;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Descripcion";
            this.columnHeader2.Width = 273;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Tiempo";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Estado";
            this.columnHeader4.Width = 75;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Claves";
            this.columnHeader6.Width = 85;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "variables";
            this.columnHeader5.Width = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel8.Location = new System.Drawing.Point(318, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(2, 100);
            this.panel8.TabIndex = 47;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::TrabajaHOY.Properties.Resources.TrabajaHoyLogo;
            this.pictureBox1.Location = new System.Drawing.Point(88, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(224, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 46;
            this.pictureBox1.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel7.BackgroundImage = global::TrabajaHOY.Properties.Resources.brightYellow;
            this.panel7.Controls.Add(this.logConf);
            this.panel7.Controls.Add(this.panel21);
            this.panel7.Controls.Add(this.filterCities);
            this.panel7.Controls.Add(this.daysFilterLabel);
            this.panel7.Controls.Add(this.maxRowsLogFilter);
            this.panel7.Controls.Add(this.FilterOptionsLabel);
            this.panel7.Controls.Add(this.tagsLogFilter);
            this.panel7.Controls.Add(this.tagsFilterLabel);
            this.panel7.Controls.Add(this.DateLogFilter);
            this.panel7.Controls.Add(this.toFilterLabel);
            this.panel7.Controls.Add(this.maxiumFilterLabel);
            this.panel7.Controls.Add(this.logButtonRefresh);
            this.panel7.Location = new System.Drawing.Point(319, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(621, 490);
            this.panel7.TabIndex = 45;
            // 
            // logConf
            // 
            this.logConf.AutoSize = true;
            this.logConf.BackColor = System.Drawing.Color.Transparent;
            this.logConf.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logConf.Location = new System.Drawing.Point(341, 7);
            this.logConf.Name = "logConf";
            this.logConf.Size = new System.Drawing.Size(187, 18);
            this.logConf.TabIndex = 76;
            this.logConf.Text = "Configuración del display";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel21.Location = new System.Drawing.Point(333, 2);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(2, 100);
            this.panel21.TabIndex = 75;
            // 
            // filterCities
            // 
            this.filterCities.AutoSize = true;
            this.filterCities.BackColor = System.Drawing.Color.Transparent;
            this.filterCities.Checked = true;
            this.filterCities.CheckState = System.Windows.Forms.CheckState.Checked;
            this.filterCities.Location = new System.Drawing.Point(341, 32);
            this.filterCities.Name = "filterCities";
            this.filterCities.Size = new System.Drawing.Size(196, 17);
            this.filterCities.TabIndex = 74;
            this.filterCities.Text = "Mostrar sólo de ciudades buscadas.";
            this.filterCities.UseVisualStyleBackColor = false;
            this.filterCities.CheckedChanged += new System.EventHandler(this.displayConfModiffied);
            // 
            // daysFilterLabel
            // 
            this.daysFilterLabel.AutoSize = true;
            this.daysFilterLabel.BackColor = System.Drawing.Color.Transparent;
            this.daysFilterLabel.Font = new System.Drawing.Font("Arial", 8.25F);
            this.daysFilterLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.daysFilterLabel.Location = new System.Drawing.Point(85, 34);
            this.daysFilterLabel.Name = "daysFilterLabel";
            this.daysFilterLabel.Size = new System.Drawing.Size(28, 14);
            this.daysFilterLabel.TabIndex = 73;
            this.daysFilterLabel.Text = "Días";
            // 
            // maxRowsLogFilter
            // 
            this.maxRowsLogFilter.Font = new System.Drawing.Font("Arial", 8.25F);
            this.maxRowsLogFilter.Location = new System.Drawing.Point(203, 31);
            this.maxRowsLogFilter.Mask = "00000";
            this.maxRowsLogFilter.Name = "maxRowsLogFilter";
            this.maxRowsLogFilter.Size = new System.Drawing.Size(43, 20);
            this.maxRowsLogFilter.TabIndex = 72;
            this.maxRowsLogFilter.Text = "10000";
            // 
            // FilterOptionsLabel
            // 
            this.FilterOptionsLabel.AutoSize = true;
            this.FilterOptionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.FilterOptionsLabel.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilterOptionsLabel.Location = new System.Drawing.Point(6, 7);
            this.FilterOptionsLabel.Name = "FilterOptionsLabel";
            this.FilterOptionsLabel.Size = new System.Drawing.Size(105, 18);
            this.FilterOptionsLabel.TabIndex = 71;
            this.FilterOptionsLabel.Text = "Filtros del log";
            // 
            // tagsLogFilter
            // 
            this.tagsLogFilter.Location = new System.Drawing.Point(203, 7);
            this.tagsLogFilter.Name = "tagsLogFilter";
            this.tagsLogFilter.Size = new System.Drawing.Size(124, 20);
            this.tagsLogFilter.TabIndex = 70;
            // 
            // tagsFilterLabel
            // 
            this.tagsFilterLabel.AutoSize = true;
            this.tagsFilterLabel.BackColor = System.Drawing.Color.Transparent;
            this.tagsFilterLabel.Font = new System.Drawing.Font("Arial", 8.25F);
            this.tagsFilterLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tagsFilterLabel.Location = new System.Drawing.Point(116, 10);
            this.tagsFilterLabel.Name = "tagsFilterLabel";
            this.tagsFilterLabel.Size = new System.Drawing.Size(81, 14);
            this.tagsFilterLabel.TabIndex = 69;
            this.tagsFilterLabel.Text = "Palabras clave:";
            // 
            // DateLogFilter
            // 
            this.DateLogFilter.Font = new System.Drawing.Font("Arial", 8.25F);
            this.DateLogFilter.Location = new System.Drawing.Point(59, 31);
            this.DateLogFilter.Mask = "00";
            this.DateLogFilter.Name = "DateLogFilter";
            this.DateLogFilter.Size = new System.Drawing.Size(20, 20);
            this.DateLogFilter.TabIndex = 68;
            this.DateLogFilter.Text = "10";
            // 
            // toFilterLabel
            // 
            this.toFilterLabel.AutoSize = true;
            this.toFilterLabel.BackColor = System.Drawing.Color.Transparent;
            this.toFilterLabel.Font = new System.Drawing.Font("Arial", 8.25F);
            this.toFilterLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toFilterLabel.Location = new System.Drawing.Point(15, 34);
            this.toFilterLabel.Name = "toFilterLabel";
            this.toFilterLabel.Size = new System.Drawing.Size(38, 14);
            this.toFilterLabel.TabIndex = 66;
            this.toFilterLabel.Text = "Hasta:";
            // 
            // maxiumFilterLabel
            // 
            this.maxiumFilterLabel.AutoSize = true;
            this.maxiumFilterLabel.BackColor = System.Drawing.Color.Transparent;
            this.maxiumFilterLabel.Font = new System.Drawing.Font("Arial", 8.25F);
            this.maxiumFilterLabel.Location = new System.Drawing.Point(145, 34);
            this.maxiumFilterLabel.Name = "maxiumFilterLabel";
            this.maxiumFilterLabel.Size = new System.Drawing.Size(52, 14);
            this.maxiumFilterLabel.TabIndex = 2;
            this.maxiumFilterLabel.Text = "Máximos:";
            // 
            // logButtonRefresh
            // 
            this.logButtonRefresh.AutoSize = true;
            this.logButtonRefresh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.logButtonRefresh.BackColor = System.Drawing.Color.Transparent;
            this.logButtonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.logButtonRefresh.Font = new System.Drawing.Font("Arial", 8.25F);
            this.logButtonRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logButtonRefresh.Location = new System.Drawing.Point(279, 30);
            this.logButtonRefresh.Name = "logButtonRefresh";
            this.logButtonRefresh.Size = new System.Drawing.Size(48, 23);
            this.logButtonRefresh.TabIndex = 1;
            this.logButtonRefresh.Text = "Filtrar";
            this.logButtonRefresh.UseVisualStyleBackColor = true;
            this.logButtonRefresh.Click += new System.EventHandler(this.logButtonRefreshClicked);
            // 
            // generalControlButton
            // 
            this.generalControlButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.generalControlButton.BackColor = System.Drawing.Color.Transparent;
            this.generalControlButton.BackgroundImage = global::TrabajaHOY.Properties.Resources.Green;
            this.generalControlButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.generalControlButton.ForeColor = System.Drawing.Color.White;
            this.generalControlButton.Location = new System.Drawing.Point(6, 8);
            this.generalControlButton.Name = "generalControlButton";
            this.generalControlButton.Size = new System.Drawing.Size(77, 32);
            this.generalControlButton.TabIndex = 19;
            this.generalControlButton.Text = "Iniciar";
            this.generalControlButton.UseVisualStyleBackColor = false;
            this.generalControlButton.Click += new System.EventHandler(this.startButtonClick);
            // 
            // updateTimer
            // 
            this.updateTimer.Interval = 1000;
            this.updateTimer.Tick += new System.EventHandler(this.Update);
            // 
            // MainWindowTopMenu
            // 
            this.MainWindowTopMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainWindowTopMenu.BackColor = System.Drawing.Color.Transparent;
            this.MainWindowTopMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.MainWindowTopMenu.Font = new System.Drawing.Font("Arial", 9F);
            this.MainWindowTopMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountToolStripMenuItem,
            this.languajeToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainWindowTopMenu.Location = new System.Drawing.Point(0, 0);
            this.MainWindowTopMenu.Name = "MainWindowTopMenu";
            this.MainWindowTopMenu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MainWindowTopMenu.Size = new System.Drawing.Size(190, 24);
            this.MainWindowTopMenu.Stretch = false;
            this.MainWindowTopMenu.TabIndex = 47;
            this.MainWindowTopMenu.Text = "menuStrip1";
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.gestiónDeCuentaToolStripMenuItem});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.accountToolStripMenuItem.Text = "Cuenta";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // gestiónDeCuentaToolStripMenuItem
            // 
            this.gestiónDeCuentaToolStripMenuItem.Name = "gestiónDeCuentaToolStripMenuItem";
            this.gestiónDeCuentaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.gestiónDeCuentaToolStripMenuItem.Text = "Gestión de cuenta";
            // 
            // languajeToolStripMenuItem
            // 
            this.languajeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spanishToolStripMenuItem,
            this.englishToolStripMenuItem});
            this.languajeToolStripMenuItem.Name = "languajeToolStripMenuItem";
            this.languajeToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.languajeToolStripMenuItem.Text = "Lenguaje";
            this.languajeToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.languajeChanged);
            // 
            // spanishToolStripMenuItem
            // 
            this.spanishToolStripMenuItem.Name = "spanishToolStripMenuItem";
            this.spanishToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.spanishToolStripMenuItem.Text = "Spanish";
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            this.englishToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.englishToolStripMenuItem.Text = "English";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.helpToolStripMenuItem.Text = "Ayuda";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel6.Location = new System.Drawing.Point(0, 25);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1200, 2);
            this.panel6.TabIndex = 48;
            // 
            // rightClickLogMenuStrip
            // 
            this.rightClickLogMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verAnuncioToolStripMenuItem,
            this.autorespuestaToolStripMenuItem,
            this.respuestaManualToolStripMenuItem});
            this.rightClickLogMenuStrip.Name = "rightClickLogMenuStrip";
            this.rightClickLogMenuStrip.Size = new System.Drawing.Size(171, 70);
            // 
            // verAnuncioToolStripMenuItem
            // 
            this.verAnuncioToolStripMenuItem.Name = "verAnuncioToolStripMenuItem";
            this.verAnuncioToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.verAnuncioToolStripMenuItem.Text = "Ver anuncio";
            this.verAnuncioToolStripMenuItem.Click += new System.EventHandler(this.menuStripVerAnuncio);
            // 
            // autorespuestaToolStripMenuItem
            // 
            this.autorespuestaToolStripMenuItem.Name = "autorespuestaToolStripMenuItem";
            this.autorespuestaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.autorespuestaToolStripMenuItem.Text = "Autorespuesta";
            this.autorespuestaToolStripMenuItem.Click += new System.EventHandler(this.menuStripAutorespuesta);
            // 
            // respuestaManualToolStripMenuItem
            // 
            this.respuestaManualToolStripMenuItem.Name = "respuestaManualToolStripMenuItem";
            this.respuestaManualToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.respuestaManualToolStripMenuItem.Text = "Respuesta manual";
            this.respuestaManualToolStripMenuItem.Click += new System.EventHandler(this.menuStripRespuestaManual);
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::TrabajaHOY.Properties.Resources.beigeBackground;
            this.panel5.Controls.Add(this.citiesBox);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.panel15);
            this.panel5.Controls.Add(this.panel16);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.maxDispersionTimeBox);
            this.panel5.Controls.Add(this.searchOption2Label);
            this.panel5.Controls.Add(this.maxUpdateTime);
            this.panel5.Controls.Add(this.netWindow);
            this.panel5.Controls.Add(this.searchOptionsLabel);
            this.panel5.Controls.Add(this.selectedWeb);
            this.panel5.Controls.Add(this.messageLabel);
            this.panel5.Controls.Add(this.responseMessage);
            this.panel5.Controls.Add(this.wordTags);
            this.panel5.Controls.Add(this.subjectlabel);
            this.panel5.Controls.Add(this.searchOption1Label);
            this.panel5.Controls.Add(this.responseSubject);
            this.panel5.Controls.Add(this.secondsLabel);
            this.panel5.Controls.Add(this.phoneLabel);
            this.panel5.Controls.Add(this.searchOption3Label);
            this.panel5.Controls.Add(this.responsePhone);
            this.panel5.Controls.Add(this.searchOption4Label);
            this.panel5.Controls.Add(this.emailLabel);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.responseMail);
            this.panel5.Controls.Add(this.responseOptionsLabel);
            this.panel5.Controls.Add(this.nameLabel);
            this.panel5.Controls.Add(this.alarmCheck);
            this.panel5.Controls.Add(this.responseName);
            this.panel5.Controls.Add(this.mailCheck);
            this.panel5.Controls.Add(this.autoRepplyCheck);
            this.panel5.Controls.Add(this.mailBox);
            this.panel5.Controls.Add(this.destacarCheck);
            this.panel5.Location = new System.Drawing.Point(0, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(256, 515);
            this.panel5.TabIndex = 46;
            // 
            // citiesBox
            // 
            this.citiesBox.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Italic);
            this.citiesBox.Location = new System.Drawing.Point(35, 148);
            this.citiesBox.Multiline = true;
            this.citiesBox.Name = "citiesBox";
            this.citiesBox.Size = new System.Drawing.Size(210, 21);
            this.citiesBox.TabIndex = 55;
            this.citiesBox.Text = "Escribir ciudades separadas por comas";
            this.citiesBox.TextChanged += new System.EventHandler(this.saveConfiguration);
            this.citiesBox.Enter += new System.EventHandler(this.countriesBoxEnterOrLeave);
            this.citiesBox.Leave += new System.EventHandler(this.displayConfModiffied);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 451);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 23);
            this.button1.TabIndex = 53;
            this.button1.Text = "Debug";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.DebugButtonClicked);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel15.Location = new System.Drawing.Point(39, 413);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(15, 2);
            this.panel15.TabIndex = 52;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel16.Location = new System.Drawing.Point(39, 295);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(2, 120);
            this.panel16.TabIndex = 51;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel11.Location = new System.Drawing.Point(14, 282);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(13, 2);
            this.panel11.TabIndex = 49;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel10.Location = new System.Drawing.Point(14, 200);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(2, 82);
            this.panel10.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label13.Location = new System.Drawing.Point(191, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "segundos";
            // 
            // maxDispersionTimeBox
            // 
            this.maxDispersionTimeBox.Font = new System.Drawing.Font("Arial", 8.25F);
            this.maxDispersionTimeBox.Location = new System.Drawing.Point(152, 53);
            this.maxDispersionTimeBox.Mask = "0000";
            this.maxDispersionTimeBox.Name = "maxDispersionTimeBox";
            this.maxDispersionTimeBox.Size = new System.Drawing.Size(32, 20);
            this.maxDispersionTimeBox.TabIndex = 44;
            this.maxDispersionTimeBox.Text = "3600";
            // 
            // searchOption2Label
            // 
            this.searchOption2Label.AutoSize = true;
            this.searchOption2Label.BackColor = System.Drawing.Color.Transparent;
            this.searchOption2Label.Font = new System.Drawing.Font("Arial", 8.25F);
            this.searchOption2Label.Location = new System.Drawing.Point(32, 56);
            this.searchOption2Label.Name = "searchOption2Label";
            this.searchOption2Label.Size = new System.Drawing.Size(119, 14);
            this.searchOption2Label.TabIndex = 43;
            this.searchOption2Label.Text = "Maximo tiempo admitido";
            // 
            // maxUpdateTime
            // 
            this.maxUpdateTime.Font = new System.Drawing.Font("Arial", 8.25F);
            this.maxUpdateTime.Location = new System.Drawing.Point(152, 30);
            this.maxUpdateTime.Mask = "00";
            this.maxUpdateTime.Name = "maxUpdateTime";
            this.maxUpdateTime.Size = new System.Drawing.Size(32, 20);
            this.maxUpdateTime.TabIndex = 42;
            this.maxUpdateTime.Text = "20";
            this.maxUpdateTime.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // netWindow
            // 
            this.netWindow.Location = new System.Drawing.Point(14, 479);
            this.netWindow.MinimumSize = new System.Drawing.Size(20, 20);
            this.netWindow.Name = "netWindow";
            this.netWindow.Size = new System.Drawing.Size(41, 21);
            this.netWindow.TabIndex = 41;
            this.netWindow.Url = new System.Uri("http://www.google.es/", System.UriKind.Absolute);
            this.netWindow.Visible = false;
            this.netWindow.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.netWindowNavigating);
            // 
            // searchOptionsLabel
            // 
            this.searchOptionsLabel.AutoSize = true;
            this.searchOptionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.searchOptionsLabel.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchOptionsLabel.Location = new System.Drawing.Point(4, 7);
            this.searchOptionsLabel.Name = "searchOptionsLabel";
            this.searchOptionsLabel.Size = new System.Drawing.Size(172, 18);
            this.searchOptionsLabel.TabIndex = 23;
            this.searchOptionsLabel.Text = "Opciones de búsqueda";
            // 
            // selectedWeb
            // 
            this.selectedWeb.Font = new System.Drawing.Font("Arial", 8.25F);
            this.selectedWeb.FormattingEnabled = true;
            this.selectedWeb.Items.AddRange(new object[] {
            "Milanuncios.com"});
            this.selectedWeb.Location = new System.Drawing.Point(112, 80);
            this.selectedWeb.Name = "selectedWeb";
            this.selectedWeb.Size = new System.Drawing.Size(133, 22);
            this.selectedWeb.TabIndex = 0;
            this.selectedWeb.SelectedIndexChanged += new System.EventHandler(this.selectedWebSiteChanged);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.BackColor = System.Drawing.Color.Transparent;
            this.messageLabel.Location = new System.Drawing.Point(62, 408);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(51, 13);
            this.messageLabel.TabIndex = 40;
            this.messageLabel.Text = "Mensaje*";
            // 
            // responseMessage
            // 
            this.responseMessage.Enabled = false;
            this.responseMessage.Font = new System.Drawing.Font("Arial", 8.25F);
            this.responseMessage.Location = new System.Drawing.Point(65, 424);
            this.responseMessage.Multiline = true;
            this.responseMessage.Name = "responseMessage";
            this.responseMessage.Size = new System.Drawing.Size(179, 76);
            this.responseMessage.TabIndex = 39;
            this.responseMessage.Text = "Hola, tengo un gran interés en su oferta de trabajo, agradecería que contactase c" +
    "onmigo si lo desea.\r\n\r\nUn saludo.\r\n";
            this.responseMessage.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // wordTags
            // 
            this.wordTags.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Italic);
            this.wordTags.Location = new System.Drawing.Point(35, 125);
            this.wordTags.Multiline = true;
            this.wordTags.Name = "wordTags";
            this.wordTags.Size = new System.Drawing.Size(210, 21);
            this.wordTags.TabIndex = 2;
            this.wordTags.Text = "Escribir palabras separadas por comas";
            this.wordTags.TextChanged += new System.EventHandler(this.saveConfiguration);
            this.wordTags.Enter += new System.EventHandler(this.wordTagsEnterOrLeave);
            this.wordTags.Leave += new System.EventHandler(this.wordTagsEnterOrLeave);
            // 
            // subjectlabel
            // 
            this.subjectlabel.AutoSize = true;
            this.subjectlabel.BackColor = System.Drawing.Color.Transparent;
            this.subjectlabel.Location = new System.Drawing.Point(62, 381);
            this.subjectlabel.Name = "subjectlabel";
            this.subjectlabel.Size = new System.Drawing.Size(44, 13);
            this.subjectlabel.TabIndex = 38;
            this.subjectlabel.Text = "Asunto*";
            // 
            // searchOption1Label
            // 
            this.searchOption1Label.AutoSize = true;
            this.searchOption1Label.BackColor = System.Drawing.Color.Transparent;
            this.searchOption1Label.Font = new System.Drawing.Font("Arial", 8.25F);
            this.searchOption1Label.Location = new System.Drawing.Point(32, 33);
            this.searchOption1Label.Name = "searchOption1Label";
            this.searchOption1Label.Size = new System.Drawing.Size(114, 14);
            this.searchOption1Label.TabIndex = 3;
            this.searchOption1Label.Text = "Intervalo de búsqueda";
            // 
            // responseSubject
            // 
            this.responseSubject.Enabled = false;
            this.responseSubject.Font = new System.Drawing.Font("Arial", 8.25F);
            this.responseSubject.Location = new System.Drawing.Point(116, 378);
            this.responseSubject.Name = "responseSubject";
            this.responseSubject.Size = new System.Drawing.Size(129, 20);
            this.responseSubject.TabIndex = 37;
            this.responseSubject.Text = "Respuesta a Oferta";
            this.responseSubject.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // secondsLabel
            // 
            this.secondsLabel.AutoSize = true;
            this.secondsLabel.BackColor = System.Drawing.Color.Transparent;
            this.secondsLabel.Font = new System.Drawing.Font("Arial", 8.25F);
            this.secondsLabel.Location = new System.Drawing.Point(190, 33);
            this.secondsLabel.Name = "secondsLabel";
            this.secondsLabel.Size = new System.Drawing.Size(55, 14);
            this.secondsLabel.TabIndex = 4;
            this.secondsLabel.Text = "segundos";
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.BackColor = System.Drawing.Color.Transparent;
            this.phoneLabel.Location = new System.Drawing.Point(62, 355);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(49, 13);
            this.phoneLabel.TabIndex = 36;
            this.phoneLabel.Text = "Telefono";
            // 
            // searchOption3Label
            // 
            this.searchOption3Label.AutoSize = true;
            this.searchOption3Label.BackColor = System.Drawing.Color.Transparent;
            this.searchOption3Label.Font = new System.Drawing.Font("Arial", 8.25F);
            this.searchOption3Label.Location = new System.Drawing.Point(32, 83);
            this.searchOption3Label.Name = "searchOption3Label";
            this.searchOption3Label.Size = new System.Drawing.Size(75, 14);
            this.searchOption3Label.TabIndex = 5;
            this.searchOption3Label.Text = "Web a buscar";
            // 
            // responsePhone
            // 
            this.responsePhone.Enabled = false;
            this.responsePhone.Font = new System.Drawing.Font("Arial", 8.25F);
            this.responsePhone.Location = new System.Drawing.Point(116, 352);
            this.responsePhone.Name = "responsePhone";
            this.responsePhone.Size = new System.Drawing.Size(129, 20);
            this.responsePhone.TabIndex = 35;
            this.responsePhone.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // searchOption4Label
            // 
            this.searchOption4Label.AutoSize = true;
            this.searchOption4Label.BackColor = System.Drawing.Color.Transparent;
            this.searchOption4Label.Font = new System.Drawing.Font("Arial", 8.25F);
            this.searchOption4Label.Location = new System.Drawing.Point(32, 108);
            this.searchOption4Label.Name = "searchOption4Label";
            this.searchOption4Label.Size = new System.Drawing.Size(183, 14);
            this.searchOption4Label.TabIndex = 6;
            this.searchOption4Label.Text = "Palabras clave y ciudades a buscar:";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.BackColor = System.Drawing.Color.Transparent;
            this.emailLabel.Location = new System.Drawing.Point(62, 327);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(36, 13);
            this.emailLabel.TabIndex = 34;
            this.emailLabel.Text = "Email*";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(-2, 175);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 2);
            this.panel2.TabIndex = 22;
            // 
            // responseMail
            // 
            this.responseMail.Enabled = false;
            this.responseMail.Font = new System.Drawing.Font("Arial", 8.25F);
            this.responseMail.Location = new System.Drawing.Point(115, 324);
            this.responseMail.Name = "responseMail";
            this.responseMail.Size = new System.Drawing.Size(129, 20);
            this.responseMail.TabIndex = 33;
            this.responseMail.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // responseOptionsLabel
            // 
            this.responseOptionsLabel.AutoSize = true;
            this.responseOptionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.responseOptionsLabel.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.responseOptionsLabel.Location = new System.Drawing.Point(3, 180);
            this.responseOptionsLabel.Name = "responseOptionsLabel";
            this.responseOptionsLabel.Size = new System.Drawing.Size(173, 18);
            this.responseOptionsLabel.TabIndex = 24;
            this.responseOptionsLabel.Text = "Opciones de respuesta";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.BackColor = System.Drawing.Color.Transparent;
            this.nameLabel.Location = new System.Drawing.Point(62, 299);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(48, 13);
            this.nameLabel.TabIndex = 32;
            this.nameLabel.Text = "Nombre*";
            // 
            // alarmCheck
            // 
            this.alarmCheck.AutoSize = true;
            this.alarmCheck.BackColor = System.Drawing.Color.Transparent;
            this.alarmCheck.Location = new System.Drawing.Point(34, 203);
            this.alarmCheck.Name = "alarmCheck";
            this.alarmCheck.Size = new System.Drawing.Size(85, 17);
            this.alarmCheck.TabIndex = 25;
            this.alarmCheck.Text = "Emitir alarma";
            this.alarmCheck.UseVisualStyleBackColor = false;
            this.alarmCheck.CheckedChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // responseName
            // 
            this.responseName.Enabled = false;
            this.responseName.Font = new System.Drawing.Font("Arial", 8.25F);
            this.responseName.Location = new System.Drawing.Point(115, 296);
            this.responseName.Name = "responseName";
            this.responseName.Size = new System.Drawing.Size(129, 20);
            this.responseName.TabIndex = 31;
            this.responseName.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // mailCheck
            // 
            this.mailCheck.AutoSize = true;
            this.mailCheck.BackColor = System.Drawing.Color.Transparent;
            this.mailCheck.Location = new System.Drawing.Point(34, 252);
            this.mailCheck.Name = "mailCheck";
            this.mailCheck.Size = new System.Drawing.Size(83, 17);
            this.mailCheck.TabIndex = 26;
            this.mailCheck.Text = "Avisar Email";
            this.mailCheck.UseVisualStyleBackColor = false;
            this.mailCheck.CheckedChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // autoRepplyCheck
            // 
            this.autoRepplyCheck.AutoSize = true;
            this.autoRepplyCheck.BackColor = System.Drawing.Color.Transparent;
            this.autoRepplyCheck.Location = new System.Drawing.Point(34, 275);
            this.autoRepplyCheck.Name = "autoRepplyCheck";
            this.autoRepplyCheck.Size = new System.Drawing.Size(117, 17);
            this.autoRepplyCheck.TabIndex = 30;
            this.autoRepplyCheck.Text = "Responder a oferta";
            this.autoRepplyCheck.UseVisualStyleBackColor = false;
            this.autoRepplyCheck.CheckedChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // mailBox
            // 
            this.mailBox.Enabled = false;
            this.mailBox.Font = new System.Drawing.Font("Arial", 8.25F);
            this.mailBox.Location = new System.Drawing.Point(124, 250);
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(120, 20);
            this.mailBox.TabIndex = 27;
            this.mailBox.Text = "email@domain.com";
            this.mailBox.TextChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // destacarCheck
            // 
            this.destacarCheck.AutoSize = true;
            this.destacarCheck.BackColor = System.Drawing.Color.Transparent;
            this.destacarCheck.Location = new System.Drawing.Point(34, 228);
            this.destacarCheck.Name = "destacarCheck";
            this.destacarCheck.Size = new System.Drawing.Size(69, 17);
            this.destacarCheck.TabIndex = 28;
            this.destacarCheck.Text = "Destacar";
            this.destacarCheck.UseVisualStyleBackColor = false;
            this.destacarCheck.CheckedChanged += new System.EventHandler(this.saveConfiguration);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel12.Location = new System.Drawing.Point(14, 235);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(13, 2);
            this.panel12.TabIndex = 50;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel13.Location = new System.Drawing.Point(14, 283);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(13, 2);
            this.panel13.TabIndex = 51;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel14.Location = new System.Drawing.Point(14, 259);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(13, 2);
            this.panel14.TabIndex = 50;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel17.Location = new System.Drawing.Point(39, 330);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(15, 2);
            this.panel17.TabIndex = 53;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel18.Location = new System.Drawing.Point(39, 357);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(15, 2);
            this.panel18.TabIndex = 54;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel19.Location = new System.Drawing.Point(39, 386);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(15, 2);
            this.panel19.TabIndex = 55;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel20.Location = new System.Drawing.Point(39, 412);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(15, 2);
            this.panel20.TabIndex = 56;
            // 
            // displayTimer
            // 
            this.displayTimer.Interval = 3000;
            this.displayTimer.Tick += new System.EventHandler(this.ExecuteDisplayQueue);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TrabajaHOY.Properties.Resources.orangeBackGround;
            this.ClientSize = new System.Drawing.Size(1190, 540);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.MainWindowTopMenu);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "TrabajaHOY";
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.MainWindowTopMenu.ResumeLayout(false);
            this.MainWindowTopMenu.PerformLayout();
            this.rightClickLogMenuStrip.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox selectedWeb;
    private System.Windows.Forms.TextBox wordTags;
    private System.Windows.Forms.Label searchOption1Label;
    private System.Windows.Forms.Label secondsLabel;
    private System.Windows.Forms.Label searchOption3Label;
    private System.Windows.Forms.Label searchOption4Label;
    private System.Windows.Forms.Button generalControlButton;
    private System.Windows.Forms.ProgressBar botStateBar;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label searchOptionsLabel;
    private System.Windows.Forms.Label responseOptionsLabel;
    private System.Windows.Forms.CheckBox alarmCheck;
    private System.Windows.Forms.CheckBox mailCheck;
    private System.Windows.Forms.CheckBox destacarCheck;
    private System.Windows.Forms.TextBox mailBox;
    private System.Windows.Forms.CheckBox autoRepplyCheck;
    private System.Windows.Forms.TextBox responseName;
    private System.Windows.Forms.Label nameLabel;
    private System.Windows.Forms.Label emailLabel;
    private System.Windows.Forms.TextBox responseMail;
    private System.Windows.Forms.Label phoneLabel;
    private System.Windows.Forms.TextBox responsePhone;
    private System.Windows.Forms.Label subjectlabel;
    private System.Windows.Forms.TextBox responseSubject;
    private System.Windows.Forms.Label messageLabel;
    private System.Windows.Forms.TextBox responseMessage;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.ColumnHeader eventType;
    private System.Windows.Forms.ColumnHeader Event;
    private System.Windows.Forms.ColumnHeader From;
    private System.Windows.Forms.ColumnHeader To;
    private System.Windows.Forms.ColumnHeader Date;
    private ListViewNF logDisplay;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Label eventDisplay;
    private System.Windows.Forms.Timer updateTimer;
    private System.Windows.Forms.ColumnHeader columnHeader2;
    private System.Windows.Forms.ColumnHeader columnHeader4;
    private System.Windows.Forms.WebBrowser netWindow;
    private System.Windows.Forms.MaskedTextBox maxUpdateTime;
    private System.Windows.Forms.ColumnHeader columnHeader0;
    private System.Windows.Forms.ColumnHeader columnHeader3;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.MaskedTextBox maxDispersionTimeBox;
    private System.Windows.Forms.Label searchOption2Label;
    private System.Windows.Forms.MenuStrip MainWindowTopMenu;
    private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem languajeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem spanishToolStripMenuItem;
    private System.Windows.Forms.Panel panel6;
    private System.Windows.Forms.ToolStripMenuItem gestiónDeCuentaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
    private System.Windows.Forms.Panel panel7;
    private System.Windows.Forms.Label FilterOptionsLabel;
    private System.Windows.Forms.TextBox tagsLogFilter;
    private System.Windows.Forms.Label tagsFilterLabel;
    private System.Windows.Forms.MaskedTextBox DateLogFilter;
    private System.Windows.Forms.Label toFilterLabel;
    private System.Windows.Forms.Label maxiumFilterLabel;
    private System.Windows.Forms.Button logButtonRefresh;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.MaskedTextBox maxRowsLogFilter;
    private System.Windows.Forms.Label daysFilterLabel;
    private System.Windows.Forms.ContextMenuStrip rightClickLogMenuStrip;
    private System.Windows.Forms.ToolStripMenuItem verAnuncioToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem autorespuestaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem respuestaManualToolStripMenuItem;
    private System.Windows.Forms.ColumnHeader columnHeader6;
    private System.Windows.Forms.ColumnHeader columnHeader5;
    private System.Windows.Forms.Panel panel9;
    private System.Windows.Forms.Panel panel8;
    private System.Windows.Forms.Panel panel10;
    private System.Windows.Forms.Panel panel11;
    private System.Windows.Forms.Panel panel12;
    private System.Windows.Forms.Panel panel13;
    private System.Windows.Forms.Panel panel14;
    private System.Windows.Forms.Panel panel15;
    private System.Windows.Forms.Panel panel16;
    private System.Windows.Forms.Panel panel17;
    private System.Windows.Forms.Panel panel18;
    private System.Windows.Forms.Panel panel19;
    private System.Windows.Forms.Panel panel20;
    private System.Windows.Forms.ColumnHeader columnHeader7;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Timer displayTimer;
    private System.Windows.Forms.TextBox citiesBox;
    private System.Windows.Forms.CheckBox filterCities;
    private System.Windows.Forms.Label logConf;
    private System.Windows.Forms.Panel panel21;
}

