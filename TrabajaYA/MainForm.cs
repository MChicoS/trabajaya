﻿using System;
using System.Data.SqlServerCe;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Data;

public partial class MainForm : Form
{
    // DEFINICION DE VARIABLES
    // Variables globales
    bool botActivated = false;                           // Variable global activada si el bot se encuentra activo
    int updateCounter = 0;                               // Contador de updates (segundos) para controlar el numero de ciclos en idle
    int antiStuckTimeout = 0;                            // Contador para prevenir stuck en un evento
    int barValue = 0;                                    // Valor de la barra de progreso
    int nextPage = 1;                                    // Siguiente pagina a comprobar en el parse de anuncios
    SqlCeDataReader databaseReader = null;               // Consulta de lectura de anuncios de la DB para su tratamiento
    public static int selectedLanguajeIndex = 0;         // Lenguaje seleccionado
    string forceAutoResponseId = "";                     // Id de anuncio a responder en el siguiente ciclo (forzado manualmente)
    string[] displayQueue = new string[0] { };           // Array de cola del display para evitar cambios muy rápidos en el mismo. (ahora avanza cola cada 3s)
    // User agent ficticio para indetectabilidad del navegador
    static string fakeUserAgentString = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)";
    // Ruta del programa
    static string absoluteSoftwarePath = System.Reflection.Assembly.GetEntryAssembly().Location.Replace("TrabajaHOY.exe", "");


    // DEFINICIÓN DE CONSTANTES
    int MAX_PAGES = 20;                                      // Maximas paginas a comprobar en cada parseo
    int MAX_STUCK_TIMER = 180;                              // Maximo valor del antistuck (segundos) antes de abandonar el estado ciclico actual.
    string DYN_STRING_LOG_ADVERT_STATE_WAITING = "";        // String dinámico, estado de anuncios en el log (En espera)
    string DYN_STRING_LOG_ADVERT_STATE_ALERTED = "";        // String dinámico, estado de anuncios en el log (Alertado)
    string DYN_STRING_LOG_ADVERT_STATE_RESPONSED = "";      // String dinámico, estado de anuncios en el log (Respondido)
    string DYN_STRING_LOG_ADVERT_STATE_IGNORED = "";        // String dinámico, estado de anuncios en el log (Ignorado)
    string DYN_STRING_LOG_ADVERT_STATE_HIGHLIGHTED = "";    // String dinámico, estado de anuncios en el log (Destacado)
    string DYN_STRING_DISPLAY_READING_OFFERS = "";          // String dinámico del display ("Leyendo ofertas");
    string DYN_STRING_DISPLAY_NO_WEB_SELECTED = "";         // String dinámico del display ("No se ha seleccionado ninguna web");
    string DYN_STRING_DISPLAY_WAITING_FOR_EVENTS = "";      // String dinámico del display ("Esperando eventos");
    string DYN_STRING_DISPLAY_SAVING_OFFERS = "";           // String dinámico del display ("Guardando ofertas");
    string DYN_STRING_DISPLAY_SENDING_EMAIL_TO = "";        // String dinámico del display ("Enviando email a:");
    string DYN_STRING_DISPLAY_HIGHLIGHTING_ADVERT = "";          // String dinámico del display ("Enviando sms a:");
    string DYN_STRING_DISPLAY_ADVERTISEMENT_ALERT = "";     // String dinámico del display ("Alerta de anuncio:");
    string DYN_STRING_DISPLAY_LOOKING_RELEVANT_ADVERTS = "";// String dinámico del display ("Buscando anuncios relevantes");
    string DYN_STRING_DAYS = "";                            // String dinánmico ("Día(s)")
    string DYN_STRING_HOURS = "";                           // String dinánmico ("Hora(s)")
    string DYN_STRING_MINS = "";                            // String dinánmico ("Min(s)")
    string DYN_STRING_SECS = "";                            // String dinánmico ("Sec(s)")
    string DYN_STRING_NOW = "";                             // String dinánmico ("Hace un instante")
    string DYN_STRING_UNKNOWN = "";                         // String dinánmico ("Desconocido")
    string DYN_STRING_AUTORENUEVA = "";                     // String dinánmico ("AutoRenueva")
    string DYN_STRING_DEFAULT_TAG_TEXT = "";                // String dinámico de la casilla de tags ("Escribir palabras separadas por comas")
    string DYN_STRING_DEFAULT_CITIES_BOX = "";           // String dinámico de la casilla de tags ("Escribir ciudades separadas por comas")
    string DYN_STRING_SOUND_EXECUTED = "";                  // String dinámico de display ("Sonido emitido")
    string DYN_STRING_MAIL_EXECUTED = "";                   // String dinámico de display ("Email enviado")
    string DYN_STRING_ADVERT_HIGHLIGHTED = "";              // String dinámico de display ("Anuncio destacado")
    string DYN_STRING_ERROR_SEARCH_INTERVAL = "";           // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_MAX_TIME = "";                  // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_NO_WEBSITE = "";                // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_MAIL = "";              // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_SMS = "";               // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_NAME = "";              // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_MAIL_2 = "";            // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_SUBJECT = "";           // String dinámico de error al iniciar bot si hay campos vacíos.
    string DYN_STRING_ERROR_INVALID_MESSAGE = "";           // String dinámico de error al iniciar bot si hay campos vacíos.


    // DEFINICIÓN DE ESTADOS
    // Definición de los estados del programa.
    enum state
    {
        STATE_IDLE = 1,                          // Cuenta logueada, bot activado y a la espera de eventos.
        STATE_WORKING_LOOKING = 2,               // Evento de busqueda de anuncios en progreso.
        STATE_WORKING_SAVING = 3,                // Evento de guardado de anuncios encontrados en la DB.
        STATE_WORKING_ANSWERING = 4,             // Evento de respuesta a los anuncios encontrados.
        STATE_WORKING_MANUAL_AUTORESPONSE = 5,   // Evento de respuesta forzada
    }

    // Subestados para eventos multifase como la autorespuesta
    enum substate
    {
        EVENT_READING_FROM_DB = 0,   // Subevento de lectura de anuncios de la DB
        EVENT_CLIENT_RESPONSES = 1,  // Subevento de lógica para respuesta y ejecución de acciones remotas (sms, mail) sobre anuncios interesantes encontrados.
        EVENT_REMOTE_RESPONSES = 2,  // Subevento para la autorespuesta por la web.
    }
    
    // Estados del anuncio
    enum advertStatus
    {
        STATUS_WAITING = 0,    // Anuncio en espera de ser tratado por la lógica de autorespuesta
        STATUS_ALERTED = 1,    // Anuncio alertado al usuario pero no respondido (opción desactivada)
        STATUS_RESPONSED = 2,  // Anuncio respondido por la web
        STATUS_IGNORED = 3,    // Anuncio ignorado por no satisfacer los filtros
        STATUS_HIGHLIGHTED = 4,  // Anuncio destacado
    }

    // Webs posibles
    enum webSites
    {
        WEBSITE_NONE = 0,
        WEBSITE_MILANUNCIOS = 1,
    }
    

    // Variables para los estados anteriores definidos
    webSites currentWebSite = webSites.WEBSITE_NONE;        // Web a leer para añadir futuro soporte
    state currentState = state.STATE_IDLE;                  // Estado actual del programa, usado para controlar los eventos
    substate currentSubState = substate.EVENT_READING_FROM_DB;       // Subestado del programa, usado para anidar estados dentro de otros: eventos multifase.

    // ARRAYS Y LISTAS
    // Cache de anuncios parseados
    public static string[,] advertisements = new string[0, 6];
    public enum advertisementsColumn
    {
        ADVERTISEMENT_ID = 0,
        ADVERTISEMENT_TITLE = 1,
        ADVERTISEMENT_DESCRIPTION = 2,
        ADVERTISEMENT_TIME_SECONDS = 3,
        ADVERTISEMENT_URL = 4,
        ADVERTISEMENT_WEBSITE = 5,
    }


    /*###############################################
    ########                                 ########
    ########   FUNCIONES DE CORE PRINCIPAL   ########
    ########                                 ########
    ###############################################*/

    // Inicialización del programa, carga de valores de configuración, anuncios de la base de datos y demás. (O)
    public MainForm()
    {
        // Cargar furmularios
        InitializeComponent();

        // Cargar configuración
        loadConfiguration();

        // Cargar strings según idioma
        updateLanguajeStrings();

        // Cargar website
        loadWebSite();

        // Cargar anuncios de la DB
        databaseReader = readAdvertisementsFromDB();

        // Cargar log
        updateLogDisplay(databaseReader, true);

        // desactivar sonidos de click del navegador al iniciar el programa.
        DisableClickSounds();

        // Arrancar timer
        displayTimer.Start();

        // Iniciar cola del display
        Array.Resize(ref displayQueue, displayQueue.Length + 1);
        displayQueue[displayQueue.Length - 1] = DYN_STRING_LOG_ADVERT_STATE_WAITING;
    }

    // Funcion de update cíclico (cada 1 seg) para eventos automáticos.
    private void Update(object sender, EventArgs e)
    {
        // Incrementar antistuck
        antiStuckTimeout++;

        // Si lleva mas de 2 minutos (MAX_STUCK_TIMER) en un evento, esta atascado, abortar
        if (antiStuckTimeout > MAX_STUCK_TIMER)
        {
            // Reiniciamos contador de idle
            updateCounter = 0;
            
            // Reiniciamos contador de stuck
            antiStuckTimeout = 0;
            
            // Reiniciamos estados
            currentState = state.STATE_IDLE;
            currentSubState = substate.EVENT_READING_FROM_DB;
        }

        switch (currentState)
        {
            case state.STATE_IDLE:
                // Incrementar contador de evento.
                updateCounter++;

                // Estamos en idle, reiniciamos contador de stuck.
                antiStuckTimeout = 0;

                // Incrementar valor de la barra de estado la cantidad justa para que se llene en la siguiente comprobación de anuncios
                barValue += (int)Math.Floor(100.0f / Double.Parse(maxUpdateTime.Text));
                if (barValue > 100)
                    barValue = 0;
                botStateBar.Value = barValue;


                // Forzar autoresponse
                if (forceAutoResponseId != "")
                {
                    // Estado de comprobacion de ofertas
                    currentState = state.STATE_WORKING_MANUAL_AUTORESPONSE;
                    break;
                }

                // EVENTO DE COMPROBACIÓN DE ANUNCIOS
                if (updateCounter >= int.Parse(maxUpdateTime.Text))
                {
                    // Establecemos valor del display
                    displayUpdate(DYN_STRING_DISPLAY_READING_OFFERS);

                    // reiniciamos contador de update
                    updateCounter = 0;

                    // reiniciamos contador de paginas a barrer
                    nextPage = 1;

                    // Navegamos a la pagina principal
                    switch (currentWebSite)
                    {
                        case webSites.WEBSITE_NONE:
                            displayUpdate(DYN_STRING_DISPLAY_NO_WEB_SELECTED);
                            break;
                        case webSites.WEBSITE_MILANUNCIOS:
                            netWindow.Navigate("http://www.milanuncios.com/ofertas-de-empleo/?pagina=1");
                            break;
                    }

                    // Estado de comprobacion de ofertas
                    currentState = state.STATE_WORKING_LOOKING;
                    currentSubState = substate.EVENT_READING_FROM_DB;

                    // Abortamos switch
                    break;
                }
                else
                {
                    // Establecemos valor del display
                    displayUpdate(DYN_STRING_DISPLAY_WAITING_FOR_EVENTS);
                }
                break;
            case state.STATE_WORKING_MANUAL_AUTORESPONSE:
                // Navegamos a la pagina principal
                switch (currentWebSite)
                {
                    case webSites.WEBSITE_NONE:
                        displayUpdate(DYN_STRING_DISPLAY_NO_WEB_SELECTED);
                        break;
                    case webSites.WEBSITE_MILANUNCIOS:
                        // Ubicamos el navegador en la URL de respuesta
                        if (netWindow.Url.ToString() != "http://www.milanuncios.com/email/enviar-mensaje.php?id=" + forceAutoResponseId) // prevención de excepciones
                            netWindow.Navigate("http://www.milanuncios.com/email/enviar-mensaje.php?id=" + forceAutoResponseId);
                        else // Ya estamos en la URL.
                        {
                            // Ejecutar respuesta
                            sendResponseToOffer();

                            // Cambiar status
                            // Actualizar valor en la DB (status y foundKeys cambiados)
                            updateResponsedAdvertisementOnDB(forceAutoResponseId, advertStatus.STATUS_RESPONSED.ToString(), "");

                            // Actualizar log
                            updateLogDisplay(readAdvertisementsFromDB(), true);

                            // Reiniciar variable
                            forceAutoResponseId = "";

                            // Pasar a idle
                            currentState = state.STATE_IDLE;
                            break;
                        }
                        break;
                }
                break;
            case state.STATE_WORKING_LOOKING: // EVENTO DE PARSE DE ANUNCIOS DE LA WEB
                switch (currentWebSite)
                {
                    case webSites.WEBSITE_NONE:
                        displayUpdate(DYN_STRING_DISPLAY_NO_WEB_SELECTED);
                        break;
                    case webSites.WEBSITE_MILANUNCIOS:
                        if (netWindow.Url.ToString() != "http://www.milanuncios.com/ofertas-de-empleo/?pagina=" + nextPage) // prevención de excepciones
                            netWindow.Navigate("http://www.milanuncios.com/ofertas-de-empleo/?pagina=" + nextPage);
                        else // Ya estamos en la URL.
                        {
                            // Parsear anuncios
                            if (parseAdvertisements()) // si ha terminado, cambiar de fase
                            {
                                // Avanzamos una página
                                nextPage++;

                                // Solo comprobar MAX_PAGES paginas en cada ciclo
                                if (nextPage > MAX_PAGES)
                                {
                                    // Reiniciar contador de páginas
                                    nextPage = 1;

                                    // Guardar anuncios encontrados que no estuvieran ya guardados
                                    currentState = state.STATE_WORKING_SAVING;
                                    currentSubState = substate.EVENT_READING_FROM_DB;
                                }
                                // en caso contrario, barremos la siguiente pagina en el siguiente update
                            }
                        }
                        break;
                }
                break;
            case state.STATE_WORKING_SAVING: // EVENTO DE GUARDADO DE ANUNCIOS EN DB
                // guardar anuncios encontrados
                if (saveAdvertisementsOnDB())
                {
                    // Establecemos valor del display
                    displayUpdate(DYN_STRING_DISPLAY_SAVING_OFFERS);

                    // Cargar anuncios de la DB
                    databaseReader = readAdvertisementsFromDB();

                    // Recargar log
                    updateLogDisplay(databaseReader, true);

                    // Cambiar al caso de respuesta
                    currentState = state.STATE_WORKING_ANSWERING;
                    currentSubState = substate.EVENT_READING_FROM_DB;
                }
                break;
            case state.STATE_WORKING_ANSWERING: // EVENTO DE AUTORESPUESTA
                // Variables generales
                string advertId = "";
                string title = "";
                string description = "";
                string readedTime = "";
                string foundKeys = "";
                string status = "";
                string foundDate = "";
                string time = "0";

                // Iteramos los estados
                switch (currentSubState)
                {
                    case substate.EVENT_READING_FROM_DB: // Fase de lectura
                        // Cargar anuncios de la DB
                        databaseReader = readAdvertisementsFromDB();

                        // Establecemos valor del display
                        displayUpdate(DYN_STRING_DISPLAY_LOOKING_RELEVANT_ADVERTS);

                        // Leemos primer campo
                        if (databaseReader.Read())
                            currentSubState = substate.EVENT_CLIENT_RESPONSES;
                        else // No hay campos para leer, hemos terminado
                        {
                            currentState = state.STATE_IDLE;
                            currentSubState = substate.EVENT_READING_FROM_DB;
                        }
                        break;
                    case substate.EVENT_CLIENT_RESPONSES: // Fase de barrido y respuestas locales
                        // Mientras haya para leer, iteramos
                        while (databaseReader.Read())
                        {
                            // Escribimos variables
                            advertId = databaseReader["advertId"].ToString();
                            title = databaseReader["title"].ToString();
                            description = databaseReader["description"].ToString();
                            readedTime = databaseReader["time"].ToString();
                            foundKeys = databaseReader["foundKeys"].ToString();
                            status = databaseReader["state"].ToString();
                            foundDate = databaseReader["foundDate"].ToString();

                            // Variable de soporte
                            time = "0";

                            // Calculamos el tiempo real que lleva el anuncio puesto
                            if (readedTime != "-1")
                            {
                                // Calculamos el tiempo real del anuncio
                                int foundDateInt = 0;
                                int readedTimeInt = 0;
                                int.TryParse(foundDate, out foundDateInt);
                                int.TryParse(readedTime, out readedTimeInt);

                                // Esta variable contiene los segundos reales que han transcurrido
                                // Segundos actuales - Segundos_al_leerlo + Tiempo_que_llevaba_publicado_al_leerlo
                                time = (GetUnixTimeNow() - foundDateInt + readedTimeInt).ToString();
                            }
                            else
                                time = "-1";

                            // Variables de control de respuesta
                            bool soundPlayed = false;
                            bool mailSent = false;
                            bool hightLighted = false;

                            // Tryparseamos las variables de tiempo (prevenir excepciones)
                            int maxDispersion = -1;
                            int advertTime = -1;
                            Int32.TryParse(maxDispersionTimeBox.Text, out maxDispersion);
                            Int32.TryParse(time, out advertTime);

                            // Parseamos tags a buscar
                            string searchTags = wordTags.Text;

                            // Leemos tags encontrados
                            foundKeys = getTagsFoundOnAdvertisement(title, description, searchTags);

                            // Parseamos ciudades a buscar
                            string searchCities = citiesBox.Text;
                            string foundCities = getCitiesFoundOnAdvertisement(title, searchCities);

                            /* CONDICIÓN DE RESPUESTA:
                               1. Tiempo que lleva puesto el anuncio es menor que el tiempo maximo tolerado (config)
                               2. El titulo o la descripción del anuncio poseen alguna de las palabras claves buscadas
                               3. El anuncio no ha sido respondido anteriormente
                            */

                            // COMPROBAMOS TODAS ELLAS
                            // El tiempo del anuncio es inferior al tolerado
                            if (advertTime < maxDispersion 
                                // Y se encuentran los strings filtrados en titulo o descripción
                                && (foundKeys.Length > 0)
                                // Y se han encontrado en la ciudad deseada
                                && (foundCities.Length > 0)
                                // Y el anuncio no ha sido respondido ya (0 en espera, 3 ignorado)
                                && (status == advertStatus.STATUS_WAITING.ToString() || status == advertStatus.STATUS_IGNORED.ToString()))
                                {
                                    // Debe responder? Decidimos desde el momento cero para los reportes
                                    bool mustResponse = autoRepplyCheck.Checked && validateAutoResponse();

                                    // Destacar anuncio
                                    if (destacarCheck.Checked)
                                    {
                                        displayUpdate(DYN_STRING_DISPLAY_HIGHLIGHTING_ADVERT + " " + title + " (r" + advertId + ")");
                                        status = advertStatus.STATUS_HIGHLIGHTED.ToString(); // alertado.
                                        hightLighted = true;
                                    }

                                    // Emitir alarma
                                    if (alarmCheck.Checked)
                                    {
                                        displayUpdate(DYN_STRING_DISPLAY_ADVERTISEMENT_ALERT + " " + title + " (r" + advertId + ")");
                                        playSoundByType("advertisementFound", selectedLanguajeIndex);
                                        status = advertStatus.STATUS_ALERTED.ToString(); // alertado. 
                                        soundPlayed = true;
                                    }

                                    // Enviar email
                                    if (mailCheck.Checked)
                                        if (sendEmail(mailBox.Text, advertId, title, description, foundKeys, time, mustResponse))
                                        {
                                            displayUpdate(DYN_STRING_DISPLAY_SENDING_EMAIL_TO + " (" + mailBox.Text + ")" + " " + title + " (r" + advertId + ")");
                                            status = advertStatus.STATUS_ALERTED.ToString(); // alertado.
                                            mailSent = true;
                                        }

                                    // Responder anuncio
                                    if (mustResponse)
                                        if (advertId != "" && advertId != "0") // id válida
                                        {
                                            // Pasar a fase de respuesta remota y no a iteración como en el resto de casos
                                            forceAutoResponseId = advertId.ToString().Replace(" ","");
                                            currentSubState = substate.EVENT_REMOTE_RESPONSES;
                                            return;
                                        }
                                }

                            // Sólo actualizar en DB si ha habido algún cambio
                            if (soundPlayed || mailSent || hightLighted)
                            {
                                // Actualizar valor en la DB (status y foundKeys cambiados)
                                updateResponsedAdvertisementOnDB(advertId, status, foundKeys);

                                // Actualizar log
                                updateLogDisplay(readAdvertisementsFromDB(), true);
                            }
                        }

                        // Llegados a este punto no quedan más anuncios.
                        // Cerrar reader
                        databaseReader.Close();

                        // Evento principal
                        currentState = state.STATE_IDLE;
                        currentSubState = substate.EVENT_READING_FROM_DB;
                        break;
                    case substate.EVENT_REMOTE_RESPONSES: // Fase de respuestas remotas
                        // Navegamos a la pagina principal
                        switch (currentWebSite)
                        {
                            case webSites.WEBSITE_NONE:
                                displayUpdate(DYN_STRING_DISPLAY_NO_WEB_SELECTED);
                                break;
                            case webSites.WEBSITE_MILANUNCIOS:
                                // Ubicamos el navegador en la URL de respuesta
                                if (netWindow.Url.ToString() != "http://www.milanuncios.com/email/enviar-mensaje.php?id=" + forceAutoResponseId) // prevención de excepciones
                                    netWindow.Navigate("http://www.milanuncios.com/email/enviar-mensaje.php?id=" + forceAutoResponseId);
                                else // Ya estamos en la URL.
                                {
                                    // Ejecutar respuesta
                                    sendResponseToOffer();

                                    // Cambiar status
                                    status = advertStatus.STATUS_RESPONSED.ToString(); // respondido

                                    // Actualizar valor en la DB (status y foundKeys cambiados)
                                    updateResponsedAdvertisementOnDB(forceAutoResponseId, status, foundKeys);

                                    // Anular variable
                                    forceAutoResponseId = "";

                                    // Actualizar log
                                    updateLogDisplay(readAdvertisementsFromDB(), true);

                                    // Volvemos a fase de iteración
                                    currentSubState = substate.EVENT_CLIENT_RESPONSES; // Saltamos a fase de barrido y respuestas locales
                                    break;
                                }
                                break;
                        }
                        break;
                }
                break;
        }
    }


    /*#######################################################
    ########                                         ########
    ########   FUNCIONES DE INTERACCIÓN CON LA WEB   ########
    ########                                         ########
    #######################################################*/

    // LECTURA: Función para parsear anuncios
    public bool parseAdvertisements()
    {
        switch (currentWebSite)
        {
            case webSites.WEBSITE_NONE:
                break;
            case webSites.WEBSITE_MILANUNCIOS:
                // Sólo si estamos en la web correcta
                if (!netWindow.Url.ToString().Contains("http://www.milanuncios.com/ofertas-de-empleo/?pagina="))
                    return false;

                // Barremos todos los divs.
                HtmlElementCollection divs = netWindow.Document.GetElementsByTagName("div");
                for (int i = 0; i < divs.Count; i++)
                    if (divs[i].GetAttribute("classname") == "x1") // Este div es un class x1, es decir una caja de anuncio.
                    {
                        string type0 = "";
                        string type1 = "";
                        string advertId = "";
                        string title = "";
                        string description = "";
                        string timeInSeconds = "";
                        string advUrl = "";

                        HtmlElementCollection divs2 = divs[i].GetElementsByTagName("div");
                        // PARSEO DE ID Y TIEMPO
                        // Barremos los divs en busca de la clase x2
                        for (int j = 0; j < divs2.Count; j++)
                            if (divs2[j].GetAttribute("classname") == "x2")
                            {
                                // Barremos los divs anteriores en busca de la clase x5
                                HtmlElementCollection divs3 = divs2[j].GetElementsByTagName("div");
                                for (int k = 0; k < divs3.Count; k++)
                                {
                                    // PARSEO DE TYPE0
                                    if (divs3[k].GetAttribute("classname") == "x3")
                                    {
                                        // Y este campo sí contiene el valor deseado (id)
                                        type0 = divs3[k].InnerText;
                                    }

                                    // PARSEO DE TYPE1
                                    if (divs3[k].GetAttribute("classname") == "x4")
                                    {
                                        // Y este campo sí contiene el valor deseado (id)
                                        type1 = divs3[k].InnerText;
                                    }

                                    // PARSEO DE ID
                                    if (divs3[k].GetAttribute("classname") == "x5")
                                    {
                                        // Y este campo sí contiene el valor deseado (id)
                                        advertId = divs3[k].InnerText;
                                        advertId = advertId.Replace("r", "");
                                    }

                                    // PARSEO DE TIEMPO
                                    if (divs3[k].GetAttribute("classname") == "x6")
                                    {
                                        // Y este campo sí contiene el valor deseado (tiempo)
                                        string parsedText = divs3[k].InnerText;

                                        // Los autorenueva siempre deben mostrarse arriba
                                        if (parsedText == "DESTACADO")
                                            timeInSeconds = "-1";
                                        else
                                        {
                                            // Calculamos de ahi los segundos
                                            // Extraemos el numero
                                            int timeValue = int.Parse(Regex.Replace(parsedText, "[^0-9.]", ""));

                                            int multiplier = 1;
                                            if (parsedText.Contains("sec"))
                                                multiplier = 1;
                                            else if (parsedText.Contains("min"))
                                                multiplier = 60;
                                            else if (parsedText.Contains("hora"))
                                                multiplier = 3600;
                                            else if (parsedText.Contains("dia"))
                                                multiplier = 86400;
                                            else if (parsedText.Contains("mes"))
                                                multiplier = 2592000;

                                            // Multiplicamos por la unidad parseada
                                            timeValue *= multiplier;

                                            // Variable final de salida
                                            timeInSeconds = timeValue.ToString();
                                        }
                                    }
                                }
                            }

                        // PARSEO DE TITULO Y DESCRIPCION
                        // Barremos los divs en busca de la clase x9
                        for (int j = 0; j < divs2.Count; j++)
                            if (divs2[j].GetAttribute("classname") == "x7" || divs2[j].GetAttribute("classname") == "x9") // Los que tienen foto es x7, los que no, x9.
                            {
                                // PARSEO DE TITULO
                                // Barremos los divs anteriores en busca de la clase cti
                                HtmlElementCollection divs3 = divs2[j].GetElementsByTagName("a");
                                for (int k = 0; k < divs3.Count; k++)
                                    if (divs3[k].GetAttribute("classname") == "cti")
                                    {
                                        // Y este campo sí contiene el valor deseado (title)
                                        title = divs3[k].InnerText;
                                        advUrl = divs3[k].GetAttribute("href");
                                        break;
                                    }

                                // PARSEO DE DESCRIPCION
                                divs3 = divs2[j].GetElementsByTagName("div");
                                for (int k = 0; k < divs3.Count; k++)
                                    if (divs3[k].GetAttribute("classname") == "tx")
                                    {
                                        // Y este campo sí contiene el valor deseado (descripcion)
                                        description = divs3[k].InnerText;
                                        break;
                                    }
                            }


                        // Si hay algun campo vacio, no se ha parseado bien, ignorar hasta el siguiente intento.
                        if (advertId == ""
                            || title == ""
                            || description == ""
                            || timeInSeconds == ""
                            || advUrl == ""
                            )
                            continue;

                        // Incrementar array en una unidad por cada anuncio encontrado
                        ResizeArray(ref advertisements, advertisements.GetLength(0) + 1, advertisements.GetLength(1));

                        // Insertamos el anuncio en la ultima posicion del array
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_ID] = advertId;
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_TITLE] = type0 + " " + type1 + ": " + title;
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_DESCRIPTION] = description;
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_TIME_SECONDS] = timeInSeconds;
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_URL] = advUrl;
                        advertisements[advertisements.GetLength(0) - 1, (int)advertisementsColumn.ADVERTISEMENT_WEBSITE] = (currentWebSite).ToString();
                    }
                break;
        }
        return true;
    }

    // ESCRITURA: Función para responder a una oferta. (O)
    public void sendResponseToOffer()
    {
        switch (currentWebSite)
        {
            case webSites.WEBSITE_NONE:
                break;
            case webSites.WEBSITE_MILANUNCIOS:
                // Sólo si estamos en la web correcta
                if (!netWindow.Url.ToString().Contains("enviar-mensaje.php?id=")) // prevención de excepciones
                    return;

                // establecer valores de casillas
                netWindow.Document.GetElementById("nombre").SetAttribute("value", responseName.Text);
                netWindow.Document.GetElementById("email").SetAttribute("value", responseMail.Text);
                netWindow.Document.GetElementById("repemail").SetAttribute("value", responseMail.Text);
                netWindow.Document.GetElementById("tfno").SetAttribute("value", responsePhone.Text);
                netWindow.Document.GetElementById("mensaje").SetAttribute("value", responseMessage.Text);

                // Pulsar boton, barremos inputs hasta encontrarlo.
                HtmlElementCollection inputs = netWindow.Document.GetElementsByTagName("input");
                for (int i = 0; i < inputs.Count; i++)
                    if (inputs[i].GetAttribute("classname") == "btnsubmit")
                    {
                        // Boton encontrado, pulsar
                        inputs[i].InvokeMember("Click");

                        // Abortar busqueda, ya hemos clickeado
                        break;
                    }
                break;
        }
    }


    /*##################################################
    ########                                    ########
    ########   FUNCIONES DE RESPUESTA EXTERNA   ########
    ########                                    ########
    ##################################################*/

    // Respuesta de enviar email (O)
    public bool sendEmail(string destEmail, string advertId, string title, string description, string foundKeys, string time, bool responsed)
    {
        // Email inválido
        if (destEmail == "" || !destEmail.Contains("@"))
            return false;

        // DEFINICIÓN DE VARIABLES
        // DATOS VARIABLES
        string originMail = "TrabajaHOY@outlook.com";
        string originMailPassword = "062qh4iuzT";
        string imageUrl = "https://www.imageupload.co.uk/images/2015/08/31/TrabajaHoyLogo.jpg";
        // STRINGS
        // Reporte de autorespuesta.
        string autoResponse = "El bot TrabajaHOY envía este mail como una alerta a un trabajo al que <b><u>" + (responsed ? "SÍ" : "NO") + "</b></u> ha respondido automáticamente.";
        // Reporte de tiempo de publicación.
        string tiempo = time == "-1" ? "Este anuncio es autorenueva." : "Este anuncio ha sido publicado hace " + GetTimeFormat(time);
        // Asunto
        string subjectString = "TrabajaHOY Alerta: " + title + " (r" + advertId + ")";
        string messageBody = "<html><body><img src= " + imageUrl + " alt='some_text'><br><br><h2>Este es un mensaje automático de alerta del bot TrabajaHOY avisandote del siguiente anuncio que te podría interesar:</h2><br><ul><li><b>" + title + " (r" + advertId + ")" + "</b></li><br>" + description + "<br><br><br><li>" + "<u>Claves encontradas</u>: <i>" + foundKeys + "</i></li><br><li><i>" + tiempo + "</i></li></ul><br><br>" + autoResponse + "<br><br>" + "Un cordial saludo.";

        // Datos de servidor
        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.Host = "smtp.live.com";
        client.EnableSsl = true;
        client.Timeout = 10000;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(originMail, originMailPassword);
        MailMessage mm = new MailMessage(originMail, destEmail, subjectString, messageBody);
        mm.BodyEncoding = UTF8Encoding.UTF8;
        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        mm.IsBodyHtml = true;

        // Intentamos enviar
        try
        {
            // Ejecutar envío
            client.Send(mm);

            // Reportamos como enviado correctamente
            return true;
        }
        catch // No se ha logrado enviar, reportar como falso
        {
            return false;
        }

    }


    /*#########################################
    ########                           ########
    ########   FUNCIONES DE DATABASE   ########
    ########                           ########
    #########################################*/

    /*############
    ## ANUNCIOS ##
    ############*/

    // Guardar los anuncios cacheados en la DB; la lógica se ocupará de que sólo se guarden los que no estén ya en la DB. (O)
    public bool saveAdvertisementsOnDB()
    {
        // Primero leemos todas las IDs que tenemos guardadas para verificar que el anuncio a guardar no está ya en la DB.
        // objeto de conexión
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=|DataDirectory|\TrabajaHOYDB.sdf");

        // definición de sentencia SQL
        string sql = "SELECT advertId FROM Anuncios";

        // apertura de la conexión
        if (!conn.State.Equals(System.Data.ConnectionState.Open))
            conn.Open();

        // consulta SELECT
        SqlCeCommand cmd = new SqlCeCommand(sql, conn);
        SqlCeDataReader reader = cmd.ExecuteReader();

        // Guardamos todas las IDs concatenandolas
        string readedValue = "";
        while (reader.Read())
            readedValue += reader["advertId"].ToString() + ", ";

        // Barremos array de anuncios
        for (int i = 0; i < advertisements.GetLength(0); i++)
        {
            // Cargamos ID de cada anuncio
            string advertId = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_ID];

            // Array vacío
            if (advertId == "")
                continue;

            // Anuncio ya guardado anteriormente
            if (readedValue.Contains(advertId))
                continue;

            // Cargamos variables secundarias
            string title = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_TITLE];
            if (title == null)
                title = "";
            string description = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_DESCRIPTION];
            if (description == null)
                description = "";
            string timeInSeconds = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_TIME_SECONDS];
            string advUrl = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_URL];
            string advWebSite = advertisements[i, (int)advertisementsColumn.ADVERTISEMENT_WEBSITE];

            // Guardar en base de datos este anuncio que no estaba ya
            // definición de sentencias SQL
            string sqlDelete = "DELETE FROM Anuncios WHERE advertId = @advertId;";
            string sqlAdd = "INSERT INTO Anuncios(advertId, title, description, time, foundKeys, state, foundDate, advUrl, advWebSite) values (@advertId, @title, @description, @time, @foundKeys, @state, @foundDate, @advUrl, @advWebSite);";

            // consulta DELETE
            cmd = new SqlCeCommand(sqlDelete, conn);
            cmd.Parameters.AddWithValue("@advertId", advertId);
            cmd.ExecuteNonQuery();

            // consulta INSERT INTO
            SqlCeCommand cmd2 = new SqlCeCommand(sqlAdd, conn);
            cmd2.Parameters.AddWithValue("@advertId", advertId);
            cmd2.Parameters.AddWithValue("@title", title);
            cmd2.Parameters.AddWithValue("@description", description);
            cmd2.Parameters.AddWithValue("@time", timeInSeconds);
            cmd2.Parameters.AddWithValue("@foundKeys", "");
            cmd2.Parameters.AddWithValue("@state", advertStatus.STATUS_WAITING.ToString()); // Al guardar por primera vez siempre es en espera
            cmd2.Parameters.AddWithValue("@foundDate", GetUnixTimeNow());
            cmd2.Parameters.AddWithValue("@advUrl", advUrl);
            cmd2.Parameters.AddWithValue("@advWebSite", advWebSite);
            cmd2.ExecuteNonQuery();
        }

        // cierre de conexión
        conn.Close();

        // Reiniciamos array de cache tras haberlo leido
        ResizeArray(ref advertisements, 0, advertisements.GetLength(1));

        return true;
    }

    // Leer los anuncios de la DB y cachearlos. (O)
    public SqlCeDataReader readAdvertisementsFromDB()
    {
        // definición de sentencias SQL y variable de salida
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=|DataDirectory|\TrabajaHOYDB.sdf");
        string sql = "SELECT * FROM Anuncios ORDER BY time ASC;";

        // apertura de la conexión
        if (!conn.State.Equals(System.Data.ConnectionState.Open))
            conn.Open();

        // consulta SELECT
        SqlCeCommand cmd = new SqlCeCommand(sql, conn);
        SqlCeDataReader reader = cmd.ExecuteReader();

        // Devolvemos el objeto de lectura
        return reader;
    }

    // Actualizar anuncio respondido en la DB. (O)
    public void updateResponsedAdvertisementOnDB(string advertId, string status, string foundKeys)
    {
        // conexión
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=|DataDirectory|\TrabajaHoyDB.sdf");

        // apertura de conexión.
        if (!conn.State.Equals(System.Data.ConnectionState.Open))
            conn.Open();

        // Consulta UPDATE
        string updateTable = "UPDATE Anuncios SET state = @state, foundKeys = @foundKeys WHERE advertId = @advertId";
        SqlCeCommand cmd = new SqlCeCommand(updateTable, conn);
        cmd.Parameters.AddWithValue("@state", status);
        cmd.Parameters.AddWithValue("@foundKeys", foundKeys);
        cmd.Parameters.AddWithValue("@advertId", advertId);
        cmd.ExecuteNonQuery();

        // Cerrar conexión
        conn.Close();
    }

    /*#################
    ## CONFIGURACIÓN ##
    #################*/

    public void displayConfModiffied(object sender, EventArgs e)
    {
        updateLogDisplay(readAdvertisementsFromDB(), true);
    }

    // Guardar configuración en DB (O)
    // TODO: Crear una columna para cada campo cuando ya estén claras todas las variables que se van a usar
    public void saveConfiguration(object sender, EventArgs e)
    {
        // actualizar enables de menu
        updateEnables();

        // Variable de guardado
        string configurationArray = "";
        configurationArray += maxUpdateTime.Text + ",THdbsplit,";
        configurationArray += maxDispersionTimeBox.Text + ",THdbsplit,";
        configurationArray += selectedWeb.SelectedIndex + ",THdbsplit,";
        configurationArray += wordTags.Text + ",THdbsplit,";
        configurationArray += alarmCheck.Checked + ",THdbsplit,";
        configurationArray += mailCheck.Checked + ",THdbsplit,";
        configurationArray += mailBox.Text + ",THdbsplit,";
        configurationArray += destacarCheck.Checked + ",THdbsplit,";
        configurationArray += autoRepplyCheck.Checked + ",THdbsplit,";
        configurationArray += responseName.Text + ",THdbsplit,";
        configurationArray += responseMail.Text + ",THdbsplit,";
        configurationArray += responsePhone.Text + ",THdbsplit,";
        configurationArray += responseSubject.Text + ",THdbsplit,";
        configurationArray += responseMessage.Text + ",THdbsplit,";
        configurationArray += selectedLanguajeIndex + ",THdbsplit,";
        configurationArray += tagsLogFilter.Text + ",THdbsplit,";
        configurationArray += DateLogFilter.Text + ",THdbsplit,";
        configurationArray += citiesBox.Text + ",THdbsplit,";
        configurationArray += filterCities.Checked + ",THdbsplit,";
        configurationArray += maxRowsLogFilter.Text;

        // objeto de conexión
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=|DataDirectory|\TrabajaHOYDB.sdf");

        // definición de sentencias SQL
        string sqlDelete = "DELETE FROM Configuration;";
        string sqlAdd = "INSERT INTO Configuration(profile, configurationArray) values (@profile, @configurationArray);";

        // apertura de la conexión
        if (!conn.State.Equals(System.Data.ConnectionState.Open))
            conn.Open();

        // consulta DELETE
        SqlCeCommand cmd = new SqlCeCommand(sqlDelete, conn);
        cmd.ExecuteNonQuery();

        // consulta INSERT INTO
        SqlCeCommand cmd2 = new SqlCeCommand(sqlAdd, conn);
        cmd2.Parameters.AddWithValue("@profile", "none");
        cmd2.Parameters.AddWithValue("@configurationArray", configurationArray);
        cmd2.ExecuteNonQuery();

        // cierre de conexión
        conn.Close();
    }

    // Cargar configuración de DB (O)
    public void loadConfiguration()
    {
        // objeto de conexión
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=|DataDirectory|\TrabajaHoyDB.sdf");

        // definición de sentencias SQL y variable de salida
        string readedValue = "";
        string sql = "SELECT configurationArray FROM Configuration";

        //SqlCeEngine engine = new SqlCeEngine("Data Source=TrabajaHoyDB.sdf");
        //engine.Upgrade();

        // apertura de la conexión
        if (!conn.State.Equals(System.Data.ConnectionState.Open))
            conn.Open();

        // consulta SELECT
        SqlCeCommand cmd = new SqlCeCommand(sql, conn);
        SqlCeDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
            readedValue = reader["configurationArray"].ToString();

        // cierre de conexión
        conn.Close();

        // cargamos configuración previa
        string[] configuration = Regex.Split(readedValue, ",THdbsplit,");


        // Cargamos los valores en los formularios con comprobación de excepción
        if (configuration.Length > 0)
            maxUpdateTime.Text = configuration[0];
        if (configuration.Length > 1)
            maxDispersionTimeBox.Text = configuration[1];
        if (configuration.Length > 2)
            selectedWeb.SelectedIndex = int.Parse(configuration[2]);
        if (configuration.Length > 3)
            wordTags.Text = configuration[3];
        if (configuration.Length > 4)
            alarmCheck.Checked = Boolean.Parse(configuration[4]);
        if (configuration.Length > 5)
            mailCheck.Checked = Boolean.Parse(configuration[5]);
        if (configuration.Length > 6)
            mailBox.Text = configuration[6];
        if (configuration.Length > 7)
            destacarCheck.Checked = Boolean.Parse(configuration[7]);
        if (configuration.Length > 8)
            autoRepplyCheck.Checked = Boolean.Parse(configuration[8]);
        if (configuration.Length > 9)
            responseName.Text = configuration[9];
        if (configuration.Length > 10)
            responseMail.Text = configuration[10];
        if (configuration.Length > 11)
            responsePhone.Text = configuration[11];
        if (configuration.Length > 12)
            responseSubject.Text = configuration[12];
        if (configuration.Length > 13)
            responseMessage.Text = configuration[13];
        if (configuration.Length > 14)
            selectedLanguajeIndex = int.Parse(configuration[14]);
        if (configuration.Length > 15)
            tagsLogFilter.Text = configuration[15];
        if (configuration.Length > 16)
            DateLogFilter.Text = configuration[16];
        if (configuration.Length > 17)
            citiesBox.Text = configuration[17];
        if (configuration.Length > 18)
            filterCities.Checked = Boolean.Parse(configuration[18]);
        if (configuration.Length > 19)
            maxRowsLogFilter.Text = configuration[19];
    }


    /*#########################################
    ########                           ########
    ########   FUNCIONES DE INTERFAZ   ########
    ########                           ########
    #########################################*/

    // Boton de iniciar pulsado (O)
    private void startButtonClick(object sender, EventArgs e)
    {
        // El bot esta activo
        if (botActivated)
        {
            // Desactivamos variable de control
            botActivated = false;

            // Paramos timer de update
            updateTimer.Stop();

            // Actualizamos apariencia del botón
            generalControlButton.Text = "Iniciar";
            generalControlButton.BackgroundImage = global::TrabajaHOY.Properties.Resources.Green;
        }
        // El bot no está activo
        else if (botCanBeActivated()) // Todos los formularios son correctos para activarse.
        {
            // Activamos variable de control
            botActivated = true;

            // Arrancamos timer de update
            updateTimer.Start();

            // Actualizamos apariencia del botón
            generalControlButton.Text = "Parar";
            generalControlButton.BackgroundImage = global::TrabajaHOY.Properties.Resources.Red;
        }
    }

    // Recarga el log de anuncios desde la DB (O)
    public void updateLogDisplay(SqlCeDataReader reader, bool ignoreFilters)
    {
        // Prevención de excepciones
        if (reader.IsClosed)
            return;

        // borrar listview anterior
        logDisplay.Items.Clear();

        // Alternancia de colores
        int colorCount = 2;
        int rowCount = 0;

        while (reader.Read())
        {
            // Cargamos variables de la DB
            string advertId = reader["advertId"].ToString();
            string title = reader["title"].ToString();
            string description = reader["description"].ToString();
            string readedTime = reader["time"].ToString();
            string foundKeys = reader["foundKeys"].ToString();
            string status = reader["state"].ToString();
            string foundDate = reader["foundDate"].ToString();
            string advUrl = reader["advUrl"].ToString();
            string webSite = reader["advWebSite"].ToString();

            // Variable de soporte
            string time = "0";

            // Calculamos el tiempo real que lleva el anuncio puesto
            if (readedTime != "-1") // Ignorar calculo para el caso -1 (autorenueva)
            {
                // Calculamos el tiempo real del anuncio
                int foundDateInt = 0;
                int readedTimeInt = 0;
                int.TryParse(foundDate, out foundDateInt);
                int.TryParse(readedTime, out readedTimeInt);

                // Esta variable contiene los segundos reales que han transcurrido
                // Segundos actuales - Segundos_al_leerlo + Tiempo_que_llevaba_publicado_al_leerlo
                time = (GetUnixTimeNow() - foundDateInt + readedTimeInt).ToString();
            }
            else
                time = "-1";

            // Comprobamos filtros en caso de que NO haya que ignorarlos
            if (!ignoreFilters)
            {
                // FILTRO DE FILAS MÁXIMAS
                if (rowCount > int.Parse(maxRowsLogFilter.Text)) // Si se cumple, abortamos busqueda directamente, no debemos mostrar más.
                    break;

                // FILTRO DE PALABRAS CLAVE
                bool tagFound = false;
                if (tagsLogFilter.Text == "") // Filtro de tags vacío, ignorar este filtro
                    tagFound = true;
                else // Hay tags para buscar, debemos comprobar si se encuentran en el anuncio
                {
                    string[] tagSplit = Regex.Split(tagsLogFilter.Text.ToLower(), ",");
                    for (int i = 0; i < tagSplit.Length; i++)
                        if (foundKeys.ToLower().Contains(tagSplit[i])) // Encontrado un tag
                            tagFound = true;
                }
                // Si no se encuentra el tag, abortar linea
                if (!tagFound)
                    continue;

                // FILTRO DE TIEMPO MÁXIMO
                // Calculamos los segundos absolutos de los dias máximos tolerados
                int secondsOrigin = int.Parse(DateLogFilter.Text) * 86400;
                // El anuncio lleva más tiempo del tolerado, abortar lineas.
                if (int.Parse(time) > secondsOrigin)
                    continue;

            }

            // FILTROS PASIVOS

            // FILTRO DE CIUDADES
            bool cityFound = false;
            if (filterCities.Checked == true) // Si el filtro no esta activo, ignorar.
            {
                // Spliteamos array de ciudades
                string[] tagSplit = Regex.Split(citiesBox.Text.ToLower(), ",");

                // Lista de ciudades vacias o con texto predefinido, ignorar
                if (tagSplit.Length == 0 || citiesBox.Text == DYN_STRING_DEFAULT_CITIES_BOX)
                    cityFound = true;
                else // Comprobamos si el titulo contiene alguna de las ciudades que buscamos
                {
                    for (int i = 0; i < tagSplit.Length; i++)
                        if (title.ToLower().Contains(tagSplit[i])) // Encontrada una ciudad
                            cityFound = true;
                }
                // Si no se encuentra la ciudad, abortar linea
                if (!cityFound)
                    continue;
            }

            // Incrementamos contador
            rowCount++;

            // Establecemos estado y color de fondo segun el status numerico leido de la DB.
            string statusText = DYN_STRING_LOG_ADVERT_STATE_WAITING;
            var backGroundColor = ColorTranslator.FromHtml("#FBFBEF");
            switch (status)
            {
                default: // Cualquier caso adicional
                case "STATUS_WAITING":
                    statusText = DYN_STRING_LOG_ADVERT_STATE_WAITING;
                    // Alternancia de colores
                    colorCount++;
                    if (colorCount % 2 == 0)
                        backGroundColor = ColorTranslator.FromHtml("#EFFBF8");
                    break;
                case "STATUS_ALERTED":
                    statusText = DYN_STRING_LOG_ADVERT_STATE_ALERTED;
                    backGroundColor = ColorTranslator.FromHtml("#D8F781"); // Verde mas amarillo
                    break;
                case "STATUS_RESPONSED":
                    statusText = DYN_STRING_LOG_ADVERT_STATE_RESPONSED;
                    backGroundColor = ColorTranslator.FromHtml("#BCFDA6"); // Verde 
                    break;
                case "STATUS_HIGHLIGHTED":
                    statusText = DYN_STRING_LOG_ADVERT_STATE_HIGHLIGHTED;
                    backGroundColor = ColorTranslator.FromHtml("#80F958"); // Verde brillante
                    break;
                case "STATUS_IGNORED":
                    statusText = DYN_STRING_LOG_ADVERT_STATE_IGNORED;
                    backGroundColor = ColorTranslator.FromHtml("#F7BE81"); // Naranja
                    break;
            }

            // Le damos formato al tiempo
            time = GetTimeFormat(time);

            // Variables de fila
            string[] logRow = { rowCount.ToString(), advertId, title, description, time, statusText, foundKeys, advUrl + "," + webSite };
            var logAdvertisement = new ListViewItem(logRow, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, (status != "STATUS_WAITING" && status != "STATUS_IGNORED" && destacarCheck.Checked ? (new System.Drawing.Font("Arial", 10.0F, FontStyle.Bold)) : new System.Drawing.Font("Arial", 9.0f)));

            // Establecemos color según acción.
            logAdvertisement.BackColor = backGroundColor;

            // Añadimos fila
            logDisplay.Items.Add(logAdvertisement);
        }
    }

    // Clickeado log de anuncios (mostrar menu desplegable al click derecho) (O)
    private void logDisplayClicked(object sender, MouseEventArgs e)
    {
        if (e.Button == MouseButtons.Right)
            if (logDisplay.FocusedItem.Bounds.Contains(e.Location) == true)
                rightClickLogMenuStrip.Show(Cursor.Position);
    }

    // Pulsado botón ver anuncio del menu strip (O)
    private void menuStripVerAnuncio(object sender, EventArgs e)
    {
        // Leemos URL guardada en la columna de variables del log (columna invisible)
        string[] varInfo = Regex.Split(logDisplay.SelectedItems[0].SubItems[7].Text, ",");

        // Leemos website
        string webSite = varInfo[1];

        // Leemos link de cada web parseado
        string paramUrl = varInfo[0];

        // Variable de soporte
        string url = "";

        // El link construido varía según la web
        switch (webSite)
        {
            case "WEBSITE_NONE":
                break;
            case "WEBSITE_MILANUNCIOS":
                url = paramUrl;
                break;
        }

        // No se ha cargado URL, abortar
        if (url == "")
            return;

        // Abrir popup con la web correcta
        var explorerPopup = new explorerPopup(url);
        explorerPopup.Show();
    }

    // Pulsado botón auto respuesta del menu strip (O)
    private void menuStripAutorespuesta(object sender, EventArgs e)
    {
        // Leemos referencia para milanuncios
        string refId = logDisplay.SelectedItems[0].SubItems[1].Text.Replace(" ","");

        // Variable global de forzar autorespuesta (será ejecutado en el siguiente instante que el programa esté en idle
        forceAutoResponseId = refId;
    }

    // Pulsado botón respuesta manual del menu strip (O)
    private void menuStripRespuestaManual(object sender, EventArgs e)
    {
        // Leemos URL guardada en la columna de variables del log (columna invisible)
        string[] varInfo = Regex.Split(logDisplay.SelectedItems[0].SubItems[7].Text, ",");

        // Leemos website
        string webSite = varInfo[1];

        // Variable de soporte
        string url = "";

        // El link construido varía según la web
        switch (webSite)
        {
            case "WEBSITE_NONE":
                break;
            case "WEBSITE_MILANUNCIOS":
                {
                    // Leemos referencia (necesario para el link de milanuncios
                    string refId = logDisplay.SelectedItems[0].SubItems[1].Text;

                    // Construimos URL
                    url = "www.milanuncios.com/email/enviar-mensaje.php?id=" + refId;
                }
                break;
        }

        // No se ha cargado URL, abortar
        if (url == "")
            return;

        // Abrir popup con la web correcta
        var explorerPopup = new explorerPopup(url);
        explorerPopup.Show();
    }

    // Pulsado boton de refresh log (O)
    private void logButtonRefreshClicked(object sender, EventArgs e)
    {
        // Cargar log
        updateLogDisplay(readAdvertisementsFromDB(), false);
    }

    // Cambiada web seleccionada en el combobox (O)
    private void selectedWebSiteChanged(object sender, EventArgs e)
    {
        // Cargar web elegida en variable global
        loadWebSite();

        // Guardar en su configuración correspondiente
        saveConfiguration(sender, e);
    }

    // actualizar menus habilitados y deshabilitados (O)
    public void updateEnables()
    {
        // Checkbox de automail
        mailBox.Enabled = mailCheck.Checked;

        // Checkbox de autorespuesta
        responseName.Enabled = autoRepplyCheck.Checked;
        responseMail.Enabled = autoRepplyCheck.Checked;
        responsePhone.Enabled = autoRepplyCheck.Checked;
        responseSubject.Enabled = autoRepplyCheck.Checked;
        responseMessage.Enabled = autoRepplyCheck.Checked;
    }

    // Clickeado cambio de idioma (O)
    private void languajeChanged(object sender, ToolStripItemClickedEventArgs e)
    {
        int selectedItemIndex = 0;
        switch (e.ClickedItem.Text)
        {
            case "Spanish": // Español por defecto
                selectedItemIndex = 0;
                break;
            case "English":
                selectedItemIndex = 1;
                break;
        }

        // sólo actualizar si nuevo lenguaje es distinto del anterior.
        if (selectedLanguajeIndex == selectedItemIndex)
            return;

        // Actualizar variable global
        selectedLanguajeIndex = selectedItemIndex;

        // Guardar cambios en configuración
        saveConfiguration(sender, e);

        // Actualizar textos según idioma
        updateLanguajeStrings();
    }

    // Entrando o saliendo de casilla de tags. (O)
    private void wordTagsEnterOrLeave(object sender, EventArgs e)
    {
        // TextBox
        var textBox = (TextBox)sender;

        // Esta focuseado
        if (textBox.Focused)
        {
            // El texto que hay es el default, borrarlo al entrar al control
            if (tagWordsBoxHasDefaultText())
                textBox.Text = "";
        }
        else if (textBox.Text == "") // No está focuseado y esta en blanco, poner texto modelo
            textBox.Text = DYN_STRING_DEFAULT_TAG_TEXT;
    }

    private void countriesBoxEnterOrLeave(object sender, EventArgs e)
    {
        // TextBox
        var textBox = (TextBox)sender;

        // Esta focuseado
        if (textBox.Focused)
        {
            // El texto que hay es el default, borrarlo al entrar al control
            if (countriesBoxHasDefaultText())
                textBox.Text = "";
        }
        else if (textBox.Text == "") // No está focuseado y esta en blanco, poner texto modelo
            textBox.Text = DYN_STRING_DEFAULT_CITIES_BOX;
    }

    // Pulsado boton de debug, funcion variable para debuguear funciones.
    private void DebugButtonClicked(object sender, EventArgs e)
    {
    }


    /*#################################################
    ########                                   ########
    ########   FUNCIONES VARIAS Y DE SOPORTE   ########
    ########                                   ########
    #################################################*/

    // Cambio de strings según idioma (O)
    public void updateLanguajeStrings()
    {
        // MAIN MENUSTRIP STRINGS
        // Mains
        accountToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 0];
        languajeToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 1];
        helpToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 2];
        // Subitems
        loginToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 3];
        gestiónDeCuentaToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 4];
        acercaDeToolStripMenuItem.Text = Translates.stripStringNames[selectedLanguajeIndex, 5];

        // SEARCH STRINGS
        searchOptionsLabel.Text = Translates.searchStringNames[selectedLanguajeIndex, 0];
        searchOption1Label.Text = Translates.searchStringNames[selectedLanguajeIndex, 1];
        searchOption2Label.Text = Translates.searchStringNames[selectedLanguajeIndex, 2];
        searchOption3Label.Text = Translates.searchStringNames[selectedLanguajeIndex, 3];
        searchOption4Label.Text = Translates.searchStringNames[selectedLanguajeIndex, 4];
        secondsLabel.Text = Translates.searchStringNames[selectedLanguajeIndex, 5];

        // RESPONSE STRINGS
        responseOptionsLabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 0];
        alarmCheck.Text = Translates.responsesStringNames[selectedLanguajeIndex, 1];
        mailCheck.Text = Translates.responsesStringNames[selectedLanguajeIndex, 2];
        destacarCheck.Text = Translates.responsesStringNames[selectedLanguajeIndex, 3];
        autoRepplyCheck.Text = Translates.responsesStringNames[selectedLanguajeIndex, 4];
        nameLabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 5];
        emailLabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 6];
        phoneLabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 7];
        subjectlabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 8];
        messageLabel.Text = Translates.responsesStringNames[selectedLanguajeIndex, 9];

        // FILTER STRINGS
        FilterOptionsLabel.Text = Translates.filterStringNames[selectedLanguajeIndex, 0];
        tagsFilterLabel.Text = Translates.filterStringNames[selectedLanguajeIndex, 1];
        toFilterLabel.Text = Translates.filterStringNames[selectedLanguajeIndex, 2];
        daysFilterLabel.Text = Translates.filterStringNames[selectedLanguajeIndex, 3];
        maxiumFilterLabel.Text = Translates.filterStringNames[selectedLanguajeIndex, 4];
        logButtonRefresh.Text = Translates.filterStringNames[selectedLanguajeIndex, 5];
        filterCities.Text = Translates.filterStringNames[selectedLanguajeIndex, 6];
        logConf.Text = Translates.filterStringNames[selectedLanguajeIndex, 7];

        // MAIN LOG COLUMNS STRINGS
        logDisplay.Columns[1].Text = Translates.logStringNames[selectedLanguajeIndex, 0];
        logDisplay.Columns[2].Text = Translates.logStringNames[selectedLanguajeIndex, 1];
        logDisplay.Columns[3].Text = Translates.logStringNames[selectedLanguajeIndex, 2];
        logDisplay.Columns[4].Text = Translates.logStringNames[selectedLanguajeIndex, 3];
        logDisplay.Columns[5].Text = Translates.logStringNames[selectedLanguajeIndex, 4];
        logDisplay.Columns[6].Text = Translates.logStringNames[selectedLanguajeIndex, 5];

        // RIGHTCLICK MENUSTRIP STRINGS
        rightClickLogMenuStrip.Items[0].Text = Translates.menuStripStringNames[selectedLanguajeIndex, 0];
        rightClickLogMenuStrip.Items[1].Text = Translates.menuStripStringNames[selectedLanguajeIndex, 1];
        rightClickLogMenuStrip.Items[2].Text = Translates.menuStripStringNames[selectedLanguajeIndex, 2];

        // DYNAMIC STRINGS
        DYN_STRING_LOG_ADVERT_STATE_WAITING = Translates.dynStrings[selectedLanguajeIndex, 0];
        DYN_STRING_LOG_ADVERT_STATE_ALERTED = Translates.dynStrings[selectedLanguajeIndex, 1];
        DYN_STRING_LOG_ADVERT_STATE_RESPONSED = Translates.dynStrings[selectedLanguajeIndex, 2];
        DYN_STRING_LOG_ADVERT_STATE_IGNORED = Translates.dynStrings[selectedLanguajeIndex, 3];
        DYN_STRING_LOG_ADVERT_STATE_HIGHLIGHTED = Translates.dynStrings[selectedLanguajeIndex, 4];
        DYN_STRING_DAYS = Translates.dynStrings[selectedLanguajeIndex, 5];
        DYN_STRING_HOURS = Translates.dynStrings[selectedLanguajeIndex, 6];
        DYN_STRING_MINS = Translates.dynStrings[selectedLanguajeIndex, 7];
        DYN_STRING_SECS = Translates.dynStrings[selectedLanguajeIndex, 8];
        DYN_STRING_NOW = Translates.dynStrings[selectedLanguajeIndex, 9];
        DYN_STRING_UNKNOWN = Translates.dynStrings[selectedLanguajeIndex, 10];
        DYN_STRING_AUTORENUEVA = Translates.dynStrings[selectedLanguajeIndex, 11];
        DYN_STRING_DEFAULT_TAG_TEXT = Translates.dynStrings[selectedLanguajeIndex, 12];
        DYN_STRING_SOUND_EXECUTED =  Translates.dynStrings[selectedLanguajeIndex, 13];
        DYN_STRING_MAIL_EXECUTED =  Translates.dynStrings[selectedLanguajeIndex, 14];
        DYN_STRING_ADVERT_HIGHLIGHTED =  Translates.dynStrings[selectedLanguajeIndex, 15];
        DYN_STRING_DEFAULT_CITIES_BOX = Translates.dynStrings[selectedLanguajeIndex, 16];
        // Errors
        DYN_STRING_ERROR_SEARCH_INTERVAL = Translates.errorStrings[selectedLanguajeIndex, 0];
        DYN_STRING_ERROR_MAX_TIME = Translates.errorStrings[selectedLanguajeIndex, 1];
        DYN_STRING_ERROR_NO_WEBSITE = Translates.errorStrings[selectedLanguajeIndex, 2];
        DYN_STRING_ERROR_INVALID_MAIL = Translates.errorStrings[selectedLanguajeIndex, 3];
        DYN_STRING_ERROR_INVALID_SMS = Translates.errorStrings[selectedLanguajeIndex, 4];
        DYN_STRING_ERROR_INVALID_NAME = Translates.errorStrings[selectedLanguajeIndex, 5];
        DYN_STRING_ERROR_INVALID_MAIL_2 = Translates.errorStrings[selectedLanguajeIndex, 6];
        DYN_STRING_ERROR_INVALID_SUBJECT = Translates.errorStrings[selectedLanguajeIndex, 7];
        DYN_STRING_ERROR_INVALID_MESSAGE = Translates.errorStrings[selectedLanguajeIndex, 8];
        // Display
        DYN_STRING_DISPLAY_READING_OFFERS = Translates.displayStrings[selectedLanguajeIndex, 0];
        DYN_STRING_DISPLAY_NO_WEB_SELECTED = Translates.displayStrings[selectedLanguajeIndex, 1];
        DYN_STRING_DISPLAY_WAITING_FOR_EVENTS = Translates.displayStrings[selectedLanguajeIndex, 2];
        DYN_STRING_DISPLAY_SAVING_OFFERS = Translates.displayStrings[selectedLanguajeIndex, 3];
        DYN_STRING_DISPLAY_SENDING_EMAIL_TO = Translates.displayStrings[selectedLanguajeIndex, 4];
        DYN_STRING_DISPLAY_HIGHLIGHTING_ADVERT = Translates.displayStrings[selectedLanguajeIndex, 5];
        DYN_STRING_DISPLAY_ADVERTISEMENT_ALERT = Translates.displayStrings[selectedLanguajeIndex, 6];
        DYN_STRING_DISPLAY_LOOKING_RELEVANT_ADVERTS = Translates.displayStrings[selectedLanguajeIndex, 7];

        // Si el texto actual de los tags es un default, traducir.
        if (tagWordsBoxHasDefaultText())
            wordTags.Text = DYN_STRING_DEFAULT_TAG_TEXT;
        // Si el texto actual de los tags es un default, traducir.
        if (countriesBoxHasDefaultText())
            citiesBox.Text = DYN_STRING_DEFAULT_CITIES_BOX;
    }

    // Lógica de validación de formularios (O)
    public bool botCanBeActivated()
    {
        // Variables de soporte
        int result = 0;

        // Comprobamos el valor del tiempo de busqueda (debe ser numérico)
        if (!int.TryParse(maxUpdateTime.Text, out result))
        {
            MessageBox.Show(DYN_STRING_ERROR_SEARCH_INTERVAL, "Error", MessageBoxButtons.OK);
            return false;
        }

        // Comprobamos el valor del tiempo de dispersión permitido (debe ser numérico)
        if (!int.TryParse(maxDispersionTimeBox.Text, out result))
        {
            MessageBox.Show(DYN_STRING_ERROR_MAX_TIME, "Error", MessageBoxButtons.OK);
            return false;
        }

        // No web seleccionada
        if (selectedWeb.SelectedIndex < 0)
        {
            MessageBox.Show(DYN_STRING_ERROR_NO_WEBSITE, "Error", MessageBoxButtons.OK);
            return false;
        }

        // Activado auto aviso por mail pero mail incorrecto
        if (mailCheck.Checked)
            if (mailBox.Text == "" || !mailBox.Text.Contains("@") || mailBox.Text == "email@domain.com") // vacío, sin arroba o con texto predefinido
            {
                MessageBox.Show(DYN_STRING_ERROR_INVALID_MAIL, "Error", MessageBoxButtons.OK);
                return false;
            }

        // Validar autorespuesta
        if (!validateAutoResponse())
            return false;

        // Si hemos llegado a este punto es que se puede activar.
        return true;
    }

    // Valida los campos rellenados de la autorespuesta (O)
    public bool validateAutoResponse()
    {
        // Activada autorespuesta pero campos incompletos
        if (autoRepplyCheck.Checked)
        {
            // nombre vacío
            if (responseName.Text == "")
            {
                MessageBox.Show(DYN_STRING_ERROR_INVALID_NAME, "Error", MessageBoxButtons.OK);
                return false;
            }

            // email vacío o incompleto
            if (responseMail.Text == "" || !responseMail.Text.Contains("@"))
            {
                MessageBox.Show(DYN_STRING_ERROR_INVALID_MAIL_2, "Error", MessageBoxButtons.OK);
                return false;
            }

            // Asunto vacío
            if (responseSubject.Text == "")
            {
                MessageBox.Show(DYN_STRING_ERROR_INVALID_SUBJECT, "Error", MessageBoxButtons.OK);
                return false;
            }

            // Asunto vacío
            if (responseMessage.Text == "")
            {
                MessageBox.Show(DYN_STRING_ERROR_INVALID_MESSAGE, "Error", MessageBoxButtons.OK);
                return false;
            }
        }
        return true;
    }

    // Obtener lista de tags de la casilla de palabras clave que se han encontrado en un anuncio (titulo y descripción) (O)
    public string getTagsFoundOnAdvertisement(string titleText, string descriptionText, string searchTags)
    {
        // Extraemos los tags de la casilla spliteando las comas
        string[] splittedTags = Regex.Split(searchTags.ToLower(), ",");

        // Variable de salida
        string output = "";

        // No hay palabras de busqueda
        if (splittedTags.Length == 0)
            return output;

        // Comprobamos todos los tags a ver si alguno se encuentra en el titulo
        for (int i = 0; i < splittedTags.Length; i++)
            if (titleText.ToLower().Contains(splittedTags[i])) // Palabra clave encontrada en el título
                if (!output.Contains(splittedTags[i])) // Palabra clave no habia sido encontrada antes
                    output += splittedTags[i] + ", "; // La concatenamos

        // Comprobamos todos los tags a ver si alguno se encuentra en la descripcion
        for (int i = 0; i < splittedTags.Length; i++)
            if (descriptionText.ToLower().Contains(splittedTags[i])) // Palabra clave encontrada en la descripción
                if (!output.Contains(splittedTags[i])) // Palabra clave no habia sido encontrada antes
                    output += splittedTags[i] + ", "; // La concatenamos

        // Eliminamos la última coma.
        output = output.Substring(0, Math.Max(output.Length - 2, 0));

        // Devolvemos resultado de palabras claves
        return output;
    }

    public string getCitiesFoundOnAdvertisement(string titleText, string searchTags)
    {
        // Extraemos los tags de la casilla spliteando las comas
        string[] splittedTags = Regex.Split(searchTags.ToLower(), ",");

        // Variable de salida
        string output = "";

        // No hay palabras de busqueda
        if (splittedTags.Length == 0)
            return output;

        // Comprobamos todos los tags a ver si alguno se encuentra en el titulo
        for (int i = 0; i < splittedTags.Length; i++)
            if (titleText.ToLower().Contains(splittedTags[i])) // Palabra clave encontrada en el título
                if (!output.Contains(splittedTags[i])) // Palabra clave no habia sido encontrada antes
                    output += splittedTags[i] + ", "; // La concatenamos

        // Eliminamos la última coma.
        output = output.Substring(0, Math.Max(output.Length - 2, 0));

        // Devolvemos resultado de palabras claves
        return output;
    }

    // Cargar web en el navegador en función de la elegida en el combobox de webs (O)
    public void loadWebSite()
    {
        // Desplazar navegador a la web correcta y cargar variable global
        string url = "";
        switch (selectedWeb.SelectedIndex)
        {
            case 0: // milanuncios.
                url = "http://www.milanuncios.com/ofertas-de-empleo/";
                currentWebSite = webSites.WEBSITE_MILANUNCIOS;
                break;
            case 1: // TODO
                url = "";
                currentWebSite = webSites.WEBSITE_NONE;
                break;
            case 2: // TODO
                url = "";
                currentWebSite = webSites.WEBSITE_NONE;
                break;
        }

        // Ir a la pagina en caso de haberla.
        if (url != "")
            netWindow.Navigate(url);
    }

    // redimensionar array bidimensional (O)
    public static void ResizeArray(ref string[,] Arr, int x, int y)
    {
        string[,] _arr = new string[x, y];
        int minRows = Math.Min(x, Arr.GetLength(0));
        int minCols = Math.Min(y, Arr.GetLength(1));
        for (int i = 0; i < minRows; i++)
            for (int j = 0; j < minCols; j++)
                _arr[i, j] = Arr[i, j];
        Arr = _arr;
    }

    // Reproducir sonido (O)
    public static void playSoundByType(string Type, int langIndex)
    {
        /* SOUND TYPES       
          "exceptionDetected"
          "advertisementFound"
        */
        string lang = getLangNameByIndex(langIndex);
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(absoluteSoftwarePath + "\\sounds\\" + "\\" + lang + "\\" + Type + ".wav");
        // cubrimos el sonido frente a excepciones
        try 
        {
            player.Play();
        }
        catch (System.IO.FileNotFoundException) { }
    }

    // Desactivar sonidos de click del navegador (O)
    static void DisableClickSounds()
    {
        CoInternetSetFeatureEnabled(
            FEATURE_DISABLE_NAVIGATION_SOUNDS,
            SET_FEATURE_ON_PROCESS,
            true);
    }

    // Devuelve el tiempo actual Unix en segundos (O)
    public int GetUnixTimeNow()
    {
        return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }

    // Da formato al tiempo en segundos a string (O)
    public string GetTimeFormat(string time)
    {
        // Variable de soporte
        int seconds = 0;

        // El string no es parseable
        if (!int.TryParse(time, out seconds))
            return DYN_STRING_UNKNOWN;
        else
        {
            // En milanuncios el valor -1 implica autorenueva
            if (seconds == -1)
                return DYN_STRING_AUTORENUEVA;

            // Hace un instante
            if (seconds < 2)
                return DYN_STRING_NOW;

            // En el resto de casos, convertimos a segundos
            string days = (seconds / 86400).ToString();
            string Hour = ((seconds % 86400) / 3600).ToString();
            string Min = (((seconds % 3600) % 3600) / 60).ToString();
            string Secs = (((seconds % 3600) % 3600) % 60).ToString();

            // Limpiamos strings para no mostrar ceros innecesarios
            if (days != "0")
                return days + " " + DYN_STRING_DAYS + " " + (Hour != "0" ? (Hour + " " + DYN_STRING_HOURS) : "");
            else if (Hour != "0") // Contiene horas, ignorar segundos
                return Hour + " " + DYN_STRING_HOURS + " " + (Min != "0" ? (Min + " " + DYN_STRING_MINS) : "");
            else if (Min != "0") // Contiene minutos, mostrar minutos y segundos
                return Min + " " + DYN_STRING_MINS + " " + (Secs != "0" ? (Secs + " " + DYN_STRING_SECS) : "");
            else // Sólo contiene segundos
                return Secs + " " + DYN_STRING_SECS;
        }
    }

    // Función que comprueba si el texto contenido en el textbox de tags (O)
    public bool tagWordsBoxHasDefaultText()
    {
        // Barremos todos los strings de traducciones posibles
        for (int i = 0; i < Translates.dynStrings.GetLength(0); i++)
            if (wordTags.Text == Translates.dynStrings[i, 12]) // Barremos todos los idiomas para el campo 12 que es donde está ese string
                return true; // Concide con alguno, abortar con true
        // No se encuentra coincidencia
        return false;
    }

    public bool countriesBoxHasDefaultText()
    {
        // Barremos todos los strings de traducciones posibles
        for (int i = 0; i < Translates.dynStrings.GetLength(0); i++)
            if (citiesBox.Text == Translates.dynStrings[i, 16]) // Barremos todos los idiomas para el campo 16 que es donde está ese string
                return true; // Concide con alguno, abortar con true
        // No se encuentra coincidencia
        return false;
    }

    // Devuelve el diminutivo del idioma dado su index (O)
    public static string getLangNameByIndex(int index)
    {
        // Variable de salida, por defecto español.
        string output = "es";
        switch (index)
        {
            default:
            case 0: // es
                output = "es";
                break;
            case 1: // en
                output = "en";
                break;
        }
        return output;
    }

    // Añadimos nuevo elemento a la cola de updates del display principal (O)
    public void displayUpdate(string updateText)
    {
        if (displayQueue.Length == 0
            || displayQueue[displayQueue.Length - 1] != updateText) // Añadimos el texto a la cola si el último elemento no es ya como el texto que intentamos añadir (prevenir dupes)
        {
            // Añadimos una unidad al tamaño del array
            Array.Resize(ref displayQueue, displayQueue.Length + 1);

            // Añadimos nuevo elemento a la cola
            displayQueue[displayQueue.Length - 1] = updateText;
        }
    }

    // Ejecutamos el cambio de display (ocurre cada 3 segundos) (O)
    private void ExecuteDisplayQueue(object sender, EventArgs e)
    {
        if (displayQueue.Length == 0)
            return;

        // Extraemos el primer elemento
        string updateText = displayQueue[0];

        // avanzamos la cola un puesto
        displayQueue = pushOnArray(displayQueue);

        // Mismo texto
        if (eventDisplay.Text == updateText)
            return;

        // Aplicar texto con punto y final
        eventDisplay.Text = updateText + ".";
    }

    // Elimina el primer elemento del array que le pasamos como parámetro (O)
    public string[] pushOnArray(string[] array)
    {
        string[] newAr = new string[array.Length - 1];
        for (int i = 1; i < array.Length; i++)
            newAr[i - 1] = array[i];
        Array.Resize(ref array, array.Length - 1);
        for (int k = 0; k < array.Length; k++)
            array[k] = newAr[k];
        return array;
    }

    // Navegador está navegando a una web (O)
    private void netWindowNavigating(object sender, WebBrowserNavigatingEventArgs e)
    {
        // Cambiar user agent
        changeUserAgent();

        // Eliminar errores de script
        netWindow.ScriptErrorsSuppressed = true;
    }

    // Función para cambiar el useragent del navegador para hacerlo indetectable (O)
    public static void changeUserAgent()
    {
        List<string> userAgent = new List<string>();
        string ua = fakeUserAgentString;
        UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, ua, ua.Length, 0);
    }

    /*#################
    ## CARGA DE DLLS ##
    #################*/

    // dll de soporte a user agents
    [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
    private static extern int UrlMkSetSessionOption(
        int dwOption, string pBuffer, int dwBufferLength, int dwReserved);
    const int URLMON_OPTION_USERAGENT = 0x10000001;

    // dll de desactivación de sonidos del navegador
    const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
    const int SET_FEATURE_ON_PROCESS = 0x00000002;
    [DllImport("urlmon.dll")]
    [PreserveSig]
    [return: MarshalAs(UnmanagedType.Error)]
    static extern int CoInternetSetFeatureEnabled(
        int FeatureEntry,
        [MarshalAs(UnmanagedType.U4)] int dwFlags,
        bool fEnable);
}

class ListViewNF : System.Windows.Forms.ListView
{
    // Clase de lista que no parpadea para mejor presentación (O)
    public ListViewNF()
    {
        //Activate double buffering
        this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);

        //Enable the OnNotifyMessage event so we get a chance to filter out 
        // Windows messages before they get to the form's WndProc
        this.SetStyle(ControlStyles.EnableNotifyMessage, true);
    }

    // Función de notificación para eliminar el parpadeo (O)
    protected override void OnNotifyMessage(Message m)
    {
        //Filter out the WM_ERASEBKGND message
        if (m.Msg != 0x14)
        {
            base.OnNotifyMessage(m);
        }
    }
}