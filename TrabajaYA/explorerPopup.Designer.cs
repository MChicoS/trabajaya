﻿
partial class explorerPopup
{
    /// <summary>
    /// Variable del diseñador requerida.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Limpiar los recursos que se estén utilizando.
    /// </summary>
    /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Código generado por el Diseñador de Windows Forms

    /// <summary>
    /// Método necesario para admitir el Diseñador. No se puede modificar
    /// el contenido del método con el editor de código.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(explorerPopup));
            this.netWindowPopup = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // netWindowPopup
            // 
            this.netWindowPopup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.netWindowPopup.Location = new System.Drawing.Point(0, 0);
            this.netWindowPopup.MinimumSize = new System.Drawing.Size(20, 20);
            this.netWindowPopup.Name = "netWindowPopup";
            this.netWindowPopup.Size = new System.Drawing.Size(784, 566);
            this.netWindowPopup.TabIndex = 0;
            this.netWindowPopup.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // explorerPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 566);
            this.Controls.Add(this.netWindowPopup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "explorerPopup";
            this.ShowInTaskbar = false;
            this.Text = "Navigator";
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.WebBrowser netWindowPopup;
}
